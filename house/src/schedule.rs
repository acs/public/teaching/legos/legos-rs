pub const HOUR_TICKS: u32 = 20; // 1h = 2 sek

#[derive(Debug, Copy, Clone)]
pub enum LightLevel {
    Off,
    Dimm,
    Full,
}

#[derive(Debug, Copy, Clone)]
pub struct HouseActivity {
    pub tv: bool,
    pub light: LightLevel,
    pub fridge: bool,
    pub oven: bool,
    pub power_ma: f32,
}

pub fn get_activity(tick: u32) -> HouseActivity {
    const DAYBREAK: u32 = 7 * HOUR_TICKS;
    const WORK_START: u32 = 9 * HOUR_TICKS;
    const LUNCH_START: u32 = 12 * HOUR_TICKS;
    const AFTERNOON_START: u32 = 14 * HOUR_TICKS;
    const DINNER_START: u32 = 18 * HOUR_TICKS;
    const EVENING_START: u32 = 20 * HOUR_TICKS;
    const EVENING_MIDDLE: u32 = 22 * HOUR_TICKS;
    const MIDNIGHT: u32 = 24 * HOUR_TICKS;

    // Fridges have a regular on/off cycle
    let fridge = (tick % HOUR_TICKS) > (HOUR_TICKS / 2);

    match tick % (24 * HOUR_TICKS) {
        0..DAYBREAK => HouseActivity {
            tv: false,
            light: LightLevel::Off,
            fridge,
            oven: false,
            power_ma: 50.0,
        },
        DAYBREAK..WORK_START => HouseActivity {
            tv: false,
            light: LightLevel::Off,
            fridge: true,
            oven: true,
            power_ma: 100.0,
        },
        WORK_START..LUNCH_START => HouseActivity {
            tv: false,
            light: LightLevel::Off,
            fridge,
            oven: false,
            power_ma: 150.0,
        },
        LUNCH_START..AFTERNOON_START => HouseActivity {
            tv: false,
            light: LightLevel::Off,
            fridge: true,
            oven: true,
            power_ma: 125.0,
        },
        AFTERNOON_START..DINNER_START => HouseActivity {
            tv: false,
            light: LightLevel::Off,
            fridge,
            oven: false,
            power_ma: 100.0,
        },
        DINNER_START..EVENING_START => HouseActivity {
            tv: false,
            light: LightLevel::Full,
            fridge: true,
            oven: true,
            power_ma: 125.0,
        },
        EVENING_START..EVENING_MIDDLE => HouseActivity {
            tv: true,
            light: LightLevel::Full,
            fridge,
            oven: true,
            power_ma: 100.0,
        },
        EVENING_MIDDLE..MIDNIGHT => HouseActivity {
            tv: true,
            light: LightLevel::Dimm,
            fridge,
            oven: true,
            power_ma: 75.0,
        },
        _ => unreachable!(),
    }
}
