#![no_std]
#![no_main]

use constcat::concat;
use core::{cell::RefCell, fmt::Write};
use embassy_embedded_hal::shared_bus::{asynch::i2c::I2cDevice, asynch::spi::SpiDevice};
use embassy_executor::Spawner;
use embassy_futures::select::{select, select3, Either, Either3};
use embassy_net::{IpAddress, IpEndpoint};
use embassy_sync::{
    blocking_mutex::{raw::CriticalSectionRawMutex, Mutex as BlockingMutex},
    channel::Channel,
    mutex::Mutex,
};
use embassy_time::{Duration, Ticker, Timer};
use esp_hal::{
    analog::{
        adc::{Adc, AdcConfig, AdcPin, Attenuation},
        dac::Dac,
    },
    clock::{ClockControl, Clocks},
    dma::{Dma, DmaPriority, DmaRxBuf, DmaTxBuf, Spi2DmaChannel},
    dma_buffers,
    gpio::{self, AnyPin, GpioPin, Input, Io, Level, Output, Pins, Pull},
    i2c::I2C,
    peripherals::{Peripherals, ADC1, DAC2, DMA, I2C0, MCPWM0, RMT, SPI2, TOUCH},
    prelude::*,
    rmt::Rmt,
    rng::Rng,
    rtc_cntl::Rtc,
    spi::{
        master::{Spi, SpiDmaBus},
        FullDuplexMode, SpiMode,
    },
    system::SystemControl,
    timer::{timg::TimerGroup, ErasedTimer, OneShotTimer},
    touch::{Continous, Touch, TouchPad},
    Async,
};
use esp_hal_smartled::smartLedBuffer;
use esp_hal_smartled::SmartLedsAdapter;
use esp_println::logger::init_logger;
use fugit::HertzU32;
use heapless::String;
use legos::{
    devices::{
        as1115::AS1115,
        in_s126at_led,
        ina209::INA209,
        pca9956::{PCA9956Error, PCA9956},
        power_output::PowerOutput,
        tmp100::TMP100,
        virtual_load::VirtualLoad,
    },
    i2c::reset_devices,
    init_legos,
    network::{str_to_ipv4_cidr, str_to_ipv4_port},
    power::PowerReading,
    FlashFileReader, HtmlConfig, HttpGetConfig, IpConfig, MQTTConfig, MQTTPublicationConfig,
    MQTTSubscriptionConfig, NetworkConfig,
};
#[allow(unused_imports)]
use log::{debug, error, info, warn};
use num_traits::float::FloatCore;
use serde::{Deserialize, Serialize};
use serde_json_core::{from_str, to_string};
use smart_leds::{SmartLedsWrite, RGB8};
use static_cell::StaticCell;
use static_toml::static_toml;

mod hvac;
use hvac::{HVACMode, Hvac};
mod schedule;
use schedule::{get_activity, HouseActivity, LightLevel, HOUR_TICKS};

static_toml! {
    const CONFIG = include_toml!("../legos_cfg.toml");
}

const HOUSE_NR: usize = match option_env!("HOUSE_NR") {
    Some(s) => {
        if s.len() == 1 {
            (s.as_bytes()[0] - b'0') as usize
        } else if s.len() == 2 {
            (s.as_bytes()[0] - b'0') as usize * 10 + (s.as_bytes()[1] - b'0') as usize
        } else {
            panic!("Invalid HOUSE_NR")
        }
    }
    None => 0,
};

const HOSTNAME: &str = concat!(CONFIG.house.hostname, "-", {
    // unwrap is not const...
    match option_env!("HOUSE_NR") {
        Some(s) => s,
        None => "0",
    }
});

const CLIENT_NAME: &str = concat!("house", {
    // unwrap is not const...
    match option_env!("HOUSE_NR") {
        Some(s) => s,
        None => "0",
    }
});

static MQTT_TOPIC_PUB: StaticCell<String<64>> = StaticCell::new();
static MQTT_TOPIC_SUB: StaticCell<String<64>> = StaticCell::new();

//run fn tickers
const UPDATE_INTERVAL: Duration = Duration::from_millis(100);

const MIN_TEMP: f32 = 18.0;
const MAX_TEMP: f32 = 30.0;
const HEAT_CTRL_STEP: f32 = 0.5;
const HVAC_MAX_PWR: f32 = 70.0; // in mA
const RNG_PWR: f32 = 10.0; // Noise of power consumption in mA

const BATTERY_CAPACITY: f32 = HOUR_TICKS as f32 * 100.0 * 6.0; // Battery can provide 100 mA for 6 "hours"
/// The number (Energy production/consumption) that maxes out the led bars on the side
pub const MAX_CONSUMPTION: f32 = 250.0;

#[derive(Debug, Copy, Clone, Serialize)]
/// charge ranges from 0.0 to 100.0
enum BatteryStatus {
    NotPresent,
    Charging { charge: f32 },
    Discharging { charge: f32 },
}

#[derive(Debug, Copy, Clone, Serialize)]
/// Current state of the house. Used for control algorithms and external
/// reporting (MQTT/HTTP).
struct HouseState {
    solar_production: f32, // in mA
    power: PowerReading,
    temp: f32,
    temp_target: f32,
    battery: BatteryStatus,
    hvac: HVACMode,
}
impl HouseState {
    const fn new() -> Self {
        Self {
            solar_production: 0.0,
            power: PowerReading {
                voltage: 0.0,
                current: 0.0,
                power: 0.0,
            },
            temp: 0.0,
            temp_target: 22.0,
            battery: BatteryStatus::NotPresent,
            hvac: HVACMode::Idle,
        }
    }

    fn update(
        &mut self,
        solar_production: Option<f32>,
        power: Option<PowerReading>,
        temp: Option<f32>,
        temp_target: Option<f32>,
        battery: Option<BatteryStatus>,
    ) {
        fn replace_if_some<T>(opt: &mut T, new: Option<T>) {
            if let Some(new_val) = new {
                *opt = new_val;
            }
        }
        replace_if_some(&mut self.solar_production, solar_production);
        replace_if_some(&mut self.power, power);
        replace_if_some(&mut self.temp, temp);
        replace_if_some(&mut self.temp_target, temp_target);
        replace_if_some(&mut self.battery, battery);
    }
}

#[derive(Debug, Copy, Clone, Deserialize, Default)]
/// Datastructure for overriding control in [`House`]. Generated externally
/// (MQTT/HTTP)
struct HouseExternalControl {}
const fn _ret_true() -> bool {
    true
}

static HOUSE_STATUS: BlockingMutex<CriticalSectionRawMutex, RefCell<HouseState>> =
    BlockingMutex::new(RefCell::new(HouseState::new()));
static I2C_BUS: StaticCell<Mutex<CriticalSectionRawMutex, I2C<'_, I2C0, Async>>> =
    StaticCell::new();

static CONTROL_CHANNEL: Channel<CriticalSectionRawMutex, HouseExternalControl, 4> = Channel::new();

#[derive(Debug, PartialEq, Eq)]
enum DisplayMode {
    Current,
    Target,
}

/// Holds the handles to the houses hardware
struct House {
    battery_pin: Input<'static, GpioPin<34>>,

    pwr_meas_in: INA209<I2cDevice<'static, CriticalSectionRawMutex, I2C<'static, I2C0, Async>>>,
    pwr_meas_out: INA209<I2cDevice<'static, CriticalSectionRawMutex, I2C<'static, I2C0, Async>>>,
    as1115: AS1115<I2cDevice<'static, CriticalSectionRawMutex, I2C<'static, I2C0, Async>>>,
    tmp100_bottom: TMP100<I2cDevice<'static, CriticalSectionRawMutex, I2C<'static, I2C0, Async>>>,
    tmp100_top: TMP100<I2cDevice<'static, CriticalSectionRawMutex, I2C<'static, I2C0, Async>>>,
    pca9956: PCA9956<
        I2cDevice<'static, CriticalSectionRawMutex, I2C<'static, I2C0, Async>>,
        Output<'static, AnyPin<'static>>,
    >,

    solar_leds: SmartLedsAdapter<esp_hal::rmt::Channel<esp_hal::Blocking, 0>, { 24 * 13 + 1 }>,
    battery_leds: SmartLedsAdapter<esp_hal::rmt::Channel<esp_hal::Blocking, 1>, { 24 * 5 + 1 }>,
    rng: Rng,
    cur_activity: HouseActivity,
    display_reset_ticks: u32,
    power_out_ticks: u8,
    battery_level: f32,
    tv_led_rbgs: [RGB8; 4],
    touch_plus: TouchPad<GpioPin<4>, Continous, Async>,
    touch_minus: TouchPad<GpioPin<2>, Continous, Async>,
    adc: Adc<'static, ADC1>,
    pin_adc_pv: AdcPin<GpioPin<35>, ADC1>,

    virtual_load: VirtualLoad<'static, DAC2, Output<'static, GpioPin<25>>>,
    #[allow(clippy::type_complexity)]
    power_out: PowerOutput<
        SpiDevice<
            'static,
            CriticalSectionRawMutex,
            SpiDmaBus<'static, SPI2, Spi2DmaChannel, FullDuplexMode, Async>,
            Output<'static, GpioPin<19>>,
        >,
        SpiDevice<
            'static,
            CriticalSectionRawMutex,
            SpiDmaBus<'static, SPI2, Spi2DmaChannel, FullDuplexMode, Async>,
            Output<'static, GpioPin<21>>,
        >,
    >,
    displ_mode: DisplayMode,
    hvac: Hvac<GpioPin<17>, GpioPin<27>, GpioPin<32>>,
}
impl House {
    #[allow(clippy::too_many_arguments)]
    async fn new(
        pins: Pins,
        i2c: I2C0,
        clocks: &Clocks<'static>,
        rmt: RMT,
        rng: Rng,
        adc1: ADC1,
        dac2: DAC2,
        spi: SPI2,
        dma: DMA,
        touch: TOUCH,
        mcpwm0: MCPWM0,
        mut rtc: Rtc<'_>,
    ) -> Self {
        let mut adc_config = AdcConfig::new();
        let pin_adc_pv = adc_config.enable_pin(pins.gpio35, Attenuation::Attenuation6dB);
        let adc: Adc<ADC1> = Adc::<ADC1>::new(adc1, adc_config);

        info!("Starting I2C");
        let sda = pins.gpio5;
        let scl = pins.gpio16;
        let mut i2c_bus = I2C::new_async(i2c, sda, scl, 400.kHz(), clocks);
        reset_devices(&mut i2c_bus).await;
        //bus_map(&mut i2c_bus).await;
        let i2c_bus_mutex = Mutex::<CriticalSectionRawMutex, _>::new(i2c_bus);
        let i2c_bus_static = I2C_BUS.init(i2c_bus_mutex);
        info!("Initializing INA209s");
        let pwr_meas_in = INA209::new(I2cDevice::new(i2c_bus_static), 0x41, 0.160, 3.0)
            .await
            .unwrap();
        let pwr_meas_out = INA209::new(I2cDevice::new(i2c_bus_static), 0x40, 0.020, 16.5)
            .await
            .unwrap();

        info!("Initializing AS1115");
        let mut as1115 = AS1115::new(I2cDevice::new(i2c_bus_static), 0x0)
            .await
            .unwrap();
        as1115.set_brightness(14).await.unwrap();

        let tmp100_bottom = TMP100::new(I2cDevice::new(i2c_bus_static), 0x48)
            .await
            .unwrap();
        let tmp100_top = TMP100::new(I2cDevice::new(i2c_bus_static), 0x4C)
            .await
            .unwrap();

        let battery_pin = Input::new(pins.gpio34, Pull::Up);

        info!("Initializing WS2812");
        let rmt = Rmt::new(rmt, 80.MHz(), clocks).unwrap();

        let rmt_buffer_solar = smartLedBuffer!(13);
        let solar_leds: SmartLedsAdapter<
            esp_hal::rmt::Channel<esp_hal::Blocking, 0>,
            { 24 * 13 + 1 },
        > = SmartLedsAdapter::new(rmt.channel0, pins.gpio33, rmt_buffer_solar, clocks);
        let rmt_buffer_battery = smartLedBuffer!(5);
        let battery_leds: SmartLedsAdapter<
            esp_hal::rmt::Channel<esp_hal::Blocking, 1>,
            { 24 * 5 + 1 },
        > = SmartLedsAdapter::new(rmt.channel1, pins.gpio22, rmt_buffer_battery, clocks);

        let tv_led_rbgs = [
            RGB8 { r: 0, g: 0, b: 0 },
            RGB8 { r: 0, g: 0, b: 0 },
            RGB8 { r: 0, g: 0, b: 0 },
            RGB8 { r: 0, g: 0, b: 0 },
        ];

        info!("Initializing VirtualLoad");
        let virtual_load = VirtualLoad::new(
            Dac::new(dac2, pins.gpio26),
            7.5,
            0.6,
            Some(Output::new(pins.gpio25, Level::Low)),
        );

        let sclk = pins.gpio18;
        let mosi = pins.gpio23;
        let cs_v = pins.gpio19;
        let cs_i = pins.gpio21;

        let (rx_buffer, rx_descriptors, tx_buffer, tx_descriptors) = dma_buffers!(64);
        let dma_rx_buf = DmaRxBuf::new(rx_descriptors, rx_buffer).unwrap();
        let dma_tx_buf = DmaTxBuf::new(tx_descriptors, tx_buffer).unwrap();

        let dma_channel = Dma::new(dma).spi2channel;
        let freq: HertzU32 = 100.kHz();
        let spi_bus = Spi::new(spi, freq, SpiMode::Mode0, clocks)
            .with_pins(Some(sclk), Some(mosi), gpio::NO_PIN, gpio::NO_PIN)
            .with_dma(dma_channel.configure_for_async(false, DmaPriority::Priority0))
            .with_buffers(dma_tx_buf, dma_rx_buf);

        static SPI_BUS: StaticCell<
            Mutex<CriticalSectionRawMutex, SpiDmaBus<SPI2, Spi2DmaChannel, FullDuplexMode, Async>>,
        > = StaticCell::new();
        let spi_bus = SPI_BUS.init(Mutex::new(spi_bus));

        let cs_pin_v: Output<GpioPin<19>> = Output::new(cs_v, Level::Low);
        let spi_dev_v = SpiDevice::new(spi_bus, cs_pin_v);

        let cs_pin_i: Output<GpioPin<21>> = Output::new(cs_i, Level::Low);
        let spi_dev_i = SpiDevice::new(spi_bus, cs_pin_i);

        let power_out = PowerOutput::new(spi_dev_v, spi_dev_i, 0.0, 68000.0);

        let mut i_max_ma_array: [f32; 24] = [0.0; 24];
        i_max_ma_array[10] = 20.0 * 2.0;
        i_max_ma_array[13] = in_s126at_led::INORM_IN_S126ATB;
        i_max_ma_array[14] = in_s126at_led::INORM_IN_S126ATR;
        i_max_ma_array[15] = in_s126at_led::INORM_IN_S126ATB;
        i_max_ma_array[16] = in_s126at_led::INORM_IN_S126ATA;

        let mut pca9956 = PCA9956::new(
            I2cDevice::new(i2c_bus_static),
            0x19,
            2000.0,
            i_max_ma_array,
            None,
        )
        .await
        .unwrap();
        let duties: [u8; 24] = [80; 24];
        pca9956.set_leds(&duties).await.unwrap();
        pca9956.error_check().await.unwrap_or_else(|e| {
            if e == PCA9956Error::PCAErrorFlag {
                warn!("Some LEDs are probably broken!");
            } else {
                panic!("PCA error check failed with error: {e:?}");
            }
        });

        info!("Initializing TouchPads");
        let touch = Touch::async_mode(touch, &mut rtc, None);
        let touch_plus = TouchPad::new(pins.gpio4, &touch);
        let touch_minus = TouchPad::new(pins.gpio2, &touch);

        info!("Initializing HVAC");

        let hvac = Hvac::new(pins.gpio17, pins.gpio27, pins.gpio32, mcpwm0, clocks);

        info!("Initialization Complete");

        Self {
            battery_pin,
            pwr_meas_in,
            pwr_meas_out,
            as1115,
            tmp100_bottom,
            tmp100_top,
            pca9956,
            solar_leds,
            battery_leds,
            rng,
            tv_led_rbgs,
            touch_plus,
            touch_minus,
            adc,
            pin_adc_pv,
            virtual_load,
            power_out,
            displ_mode: DisplayMode::Current,
            hvac,
            cur_activity: get_activity(0),
            display_reset_ticks: 0,
            power_out_ticks: 0,
            battery_level: BATTERY_CAPACITY * 0.3,
        }
    }

    pub async fn startup_animation(&mut self) {
        // TODO: Add animation
        Timer::after(Duration::from_millis(1)).await;
    }

    pub async fn set_cooling_led(&mut self, enable: bool) {
        if let Err(e) = self.pca9956.set_led(13, 70 * enable as u8).await {
            error!("can't set cooling led: {e:?}");
        }
    }

    pub async fn set_heating_led(&mut self, enable: bool) {
        if let Err(e) = self.pca9956.set_led(14, 70 * enable as u8).await {
            error!("can't set heating led: {e:?}");
        }
    }

    pub async fn set_fridge_led(&mut self, enable: bool) {
        if let Err(e) = self.pca9956.set_led(15, 50 * enable as u8).await {
            error!("can't set fridge led: {e:?}");
        }
    }

    pub async fn set_oven_led(&mut self, enable: bool) {
        if let Err(e) = self.pca9956.set_led(16, 50 * enable as u8).await {
            error!("can't set oven led: {e:?}");
        }
    }

    pub async fn set_ambient_led(&mut self, level: LightLevel) {
        let duty = match level {
            LightLevel::Off => 0,
            LightLevel::Dimm => 100,
            LightLevel::Full => 255,
        };
        if let Err(e) = self.pca9956.set_led(10, duty).await {
            error!("can't set ambient led to {duty}: {e:?}");
        }
    }

    async fn turn_off_lights(&mut self) {
        let _ = self.pca9956.set_led(13, 0).await;
        let _ = self.pca9956.set_led(14, 0).await;
        let _ = self.pca9956.set_led(15, 0).await;
        let _ = self.pca9956.set_led(16, 0).await;
        let _ = self.pca9956.set_led(10, 0).await;
        self.solar_leds
            .write([RGB8::default(); 13].iter().cloned())
            .unwrap();
        // TODO: disable temp. display
    }

    pub async fn heat_control(&mut self) {
        let (cur_tmp, target_tmp) = HOUSE_STATUS.lock(|s| {
            let s = s.borrow_mut();
            (s.temp, s.temp_target)
        });

        let hvac_mode = if cur_tmp > target_tmp + 0.5 {
            HVACMode::Cool
        } else if cur_tmp < target_tmp - 0.5 {
            HVACMode::Heat
        } else {
            HVACMode::Idle
        };

        let (h_led, c_led) = match hvac_mode {
            HVACMode::Heat => (true, false),
            HVACMode::Cool => (false, true),
            HVACMode::Idle => (false, false),
        };
        self.set_heating_led(h_led).await;
        self.set_cooling_led(c_led).await;
        self.hvac.set_mode(hvac_mode);
    }

    pub async fn read_temperature(&mut self) -> Option<f32> {
        let tb = self.tmp100_bottom.read_temperature().await;
        let tt = self.tmp100_top.read_temperature().await;

        match (tb, tt) {
            (Ok(tb), Ok(tt)) => {
                Some((tt as f32 + tb as f32) / 2.0 - CONFIG.house.temp_offsets[HOUSE_NR] as f32)
            }
            (Err(e), Ok(_)) => {
                error!("Failure when reading tmp100 bottom: {e:?}");
                None
            }
            (Ok(_), Err(e)) => {
                error!("Failure when reading tmp100 top: {e:?}");
                None
            }
            (Err(e1), Err(e2)) => {
                error!("Failure when reading tmp100s: {e1:?} - {e2:?}");
                None
            }
        }
    }

    pub async fn display_temperature(&mut self) {
        if self.display_reset_ticks > 0 {
            self.displ_mode = DisplayMode::Target;
            self.display_reset_ticks -= 1;
        } else {
            self.displ_mode = DisplayMode::Current;
        }
        let nr = HOUSE_STATUS.lock(|s| match self.displ_mode {
            DisplayMode::Target => s.borrow().temp_target,
            DisplayMode::Current => s.borrow().temp,
        });
        // TODO: Display the target temperature blinking
        self.as1115
            .display_decimal_number(nr)
            .await
            .unwrap_or_else(|e| debug!("AS1115 returned an error {e:?}, tried to display {nr}"));
    }

    fn print_status(&mut self) {
        let s = HOUSE_STATUS.lock(|s: &RefCell<HouseState>| *s.borrow());
        info!("{s:#?}");
    }

    /// production & consumption must be between 0.0 and 1.0
    async fn set_side_leds(&mut self, production: f32, consumption: f32, update_tv: bool) {
        assert!(
            (0.0..=1.0).contains(&production),
            "invalid production value: {production}"
        );
        assert!(
            (0.0..=1.0).contains(&consumption),
            "invalid consumption value: {consumption}"
        );

        const NR_SIDE_LEDS: usize = 9;
        const NR_TV_LEDS: usize = 4;
        const BRIGHTNSS_RED: u8 = 0x20;
        const BRIGHTNSS_GREEN: u8 = 0x20;

        let mut data: [RGB8; NR_SIDE_LEDS + NR_TV_LEDS] = [RGB8::default(); 13];

        for (i, d) in data.iter_mut().enumerate().take(NR_SIDE_LEDS) {
            let n_leds = NR_SIDE_LEDS as f32;
            let cons_brightness = ((consumption - (i as f32 / n_leds)) * n_leds).clamp(0.0, 1.0);
            let prod_brightness = ((production - (i as f32 / n_leds)) * n_leds).clamp(0.0, 1.0);
            let mut r = (cons_brightness * BRIGHTNSS_RED as f32) as u8;
            let mut g = (prod_brightness * BRIGHTNSS_GREEN as f32) as u8;
            if r != 0 && g != 0 {
                r = 3 * r / 2;
                g /= 2
            }
            *d = RGB8 { r, g, b: 0 };
        }

        if self.cur_activity.tv {
            for (i, d) in data
                .iter_mut()
                .enumerate()
                .skip(NR_SIDE_LEDS)
                .take(NR_TV_LEDS)
            {
                if update_tv {
                    self.tv_led_rbgs[i - NR_SIDE_LEDS] = RGB8 {
                        r: (self.rng.random() as u8 as f32 / 16.0) as u8,
                        g: (self.rng.random() as u8 as f32 / 16.0) as u8,
                        b: (self.rng.random() as u8 as f32 / 16.0 + 16.0) as u8,
                    };
                }
                *d = self.tv_led_rbgs[i - NR_SIDE_LEDS];
            }
        }

        self.solar_leds.write(data.iter().cloned()).unwrap();
    }

    async fn set_battery_leds(&mut self, bat_state: BatteryStatus) {
        let battery_percentage = match bat_state {
            BatteryStatus::NotPresent => return,
            BatteryStatus::Charging { charge } | BatteryStatus::Discharging { charge } => {
                charge / 100.0
            }
        };
        assert!(
            (0.0..=1.0).contains(&battery_percentage),
            "invalid battery_percentage value: {battery_percentage}"
        );

        const BRIGHTNSS_RED: u8 = 0x0D;
        const BRIGHTNSS_GREEN: u8 = 0x10;
        const NR_BAT_LEDS: usize = 5;

        let mut data: [RGB8; NR_BAT_LEDS] = [RGB8::default(); NR_BAT_LEDS];

        for (i, d) in data.iter_mut().enumerate().take(NR_BAT_LEDS) {
            let n_leds = NR_BAT_LEDS as f32;
            let brightness = ((battery_percentage - (i as f32 / n_leds)) * n_leds).clamp(0.0, 1.0);
            *d = RGB8 {
                r: (BRIGHTNSS_RED as f32 * brightness) as u8,
                g: (BRIGHTNSS_GREEN as f32 * brightness) as u8,
                b: 0,
            };
        }

        self.battery_leds.write(data.iter().cloned()).unwrap();
    }

    async fn read_adc(&mut self, log_output: bool) -> f32 {
        let pin_value: u16 = nb::block!(self.adc.read_oneshot(&mut self.pin_adc_pv)).unwrap();
        let adc_val = 4095 - pin_value;
        let to_volt: f32 = adc_val as f32 / 4095.0 * 3.3;
        if log_output {
            debug!("PIN VAL: {pin_value}, ADC VAL: {adc_val}, VOLT: {to_volt}");
        }
        to_volt
    }

    /// Function that handles the logic of the house. Is called every `UPDATE_INTERVAL`
    async fn logic_updates(&mut self, tick: u32) {
        self.cur_activity = get_activity(tick);

        let battery_present = self.battery_pin.is_low();

        let adc_volt = self.read_adc(false).await;
        let solar_perc = adc_volt / 3.3;
        let solar_production = solar_perc * CONFIG.house.solar_max_pwr as f32;
        if tick % 20 == 0 {
            debug!("solar_perc: {solar_perc} (adc_volt: {adc_volt})");
        }

        let temp = self.read_temperature().await;

        let (cur_tmp, target_tmp) = HOUSE_STATUS.lock(|s| {
            let s = s.borrow();
            (s.temp, s.temp_target)
        });

        const HEAT_PWR_PROPORTIONAL_RANGE: f32 = 3.0; // The temperature delta, at which the HVAC will "draw full power"
        let heating_power = ((cur_tmp - target_tmp).abs()).clamp(0.0, HEAT_PWR_PROPORTIONAL_RANGE)
            / HEAT_PWR_PROPORTIONAL_RANGE
            * HVAC_MAX_PWR;

        let pwr_rnd = self.rng.random() as u8 as f32 / 256.0 * RNG_PWR;
        let total_consumption = self.cur_activity.power_ma + heating_power + pwr_rnd;

        if tick % 20 == 0 {
            debug!(
        "solar production: {solar_production:.1}mAh,\nConsumption: house activity: {:.1}mA, pwr_rnd {pwr_rnd:.1}, heating_power: {heating_power:.1}",
        self.cur_activity.power_ma
        );
            debug!(
                "total_consumption: {total_consumption:.1} ({:.1}%)",
                total_consumption / MAX_CONSUMPTION * 100.0
            )
        }

        let mut total_power = solar_production - total_consumption;

        let battery_state = match (battery_present, total_power) {
            (true, 0.0..) => {
                if self.battery_level < BATTERY_CAPACITY - total_power {
                    if tick % 20 == 0 {
                        debug!(
                            "Charging battery with {:.0}mA/t ({:.0}/{:.0})",
                            total_power, self.battery_level, BATTERY_CAPACITY
                        );
                    }
                    self.battery_level += total_power;
                    total_power = 0.0;
                } // else: Battery full

                BatteryStatus::Charging {
                    charge: (self.battery_level / BATTERY_CAPACITY * 100.0).clamp(0.0, 100.0),
                }
            }
            (true, ..0.0) => {
                if self.battery_level > total_power {
                    if tick % 20 == 0 {
                        debug!(
                            "Discharging battery with {:.0}mA/t ({:.0}/{:.0})",
                            total_power, self.battery_level, BATTERY_CAPACITY
                        );
                    }
                    self.battery_level += total_power;
                    total_power = 0.0;
                } // else: Battery is empty
                BatteryStatus::Discharging {
                    charge: (self.battery_level / BATTERY_CAPACITY * 100.0).clamp(0.0, 100.0),
                }
            }
            (false, _) => BatteryStatus::NotPresent,
            _ => unreachable!(),
        };
        self.set_battery_leds(battery_state).await;

        match total_power {
            ..0.0 => {
                self.virtual_load.set_v_load_i(-total_power).await;
                self.power_out.set_voltage(3.45).await;
                self.power_out.set_max_current(0.0).await;
            }
            0.0 => {
                self.virtual_load.set_v_load_i(0.0).await;
                self.power_out.set_voltage(3.45).await;
                self.power_out.set_max_current(0.0).await;
            }
            _ => {
                self.virtual_load.set_v_load_i(0.0).await;
                self.power_out.set_voltage(3.900).await;
                self.power_out.set_max_current(total_power).await;
            }
        }

        //let pwr_in = self.pwr_meas_in.read_power().await.ok();
        let pwr_in = match self.pwr_meas_in.read_power().await {
            Ok(pwr_in) => pwr_in,
            Err(e) => {
                error!("cant read pwr_in: {e:?}");
                return;
            }
        };
        // TODO: use pwr_out
        let _pwr_out = match self.pwr_meas_out.read_power().await {
            Ok(pwr_out) => pwr_out,
            Err(e) => {
                error!("cant read pwr_out: {e:?}");
                return;
            }
        };

        HOUSE_STATUS.lock(|s| {
            let mut s = s.borrow_mut();
            s.update(
                Some(solar_production),
                Some(pwr_in),
                temp,
                None,
                Some(battery_state),
            );
        });

        self.heat_control().await;

        if total_power < 0.0 && (pwr_in.current + total_power / 1000.0).abs() > 0.02 {
            info!(
                "not enough power! (is {}A, must be {}A)",
                pwr_in.current,
                total_power / 1000.0
            );
            self.power_out_ticks += 1;
        } else {
            self.power_out_ticks = 0;
        }
        if self.power_out_ticks > 5 {
            self.turn_off_lights().await;
        } else {
            self.set_side_leds(
                solar_production / MAX_CONSUMPTION,
                total_consumption / MAX_CONSUMPTION,
                tick % 3 == 0,
            )
            .await;

            self.set_fridge_led(self.cur_activity.fridge).await;
            self.set_oven_led(self.cur_activity.oven).await;
            self.set_ambient_led(self.cur_activity.light).await;
            self.display_temperature().await;
        }

        if tick % 40 == 0 {
            self.print_status();
        }
    }

    pub async fn run(&mut self) {
        // approx 2/3 of the baseline reading (when not touched)
        const TOUCH_THRESH_PLUS: u16 = 140;
        const TOUCH_THRESH_MINUS: u16 = 160;

        let mut update_ticker = Ticker::every(UPDATE_INTERVAL);
        let mut tick: u32 = 0;
        let mut last_touch_tick: u32 = 0;

        loop {
            match select3(
                update_ticker.next(),
                CONTROL_CHANNEL.receive(),
                select(
                    self.touch_plus.wait_for_touch(TOUCH_THRESH_PLUS),
                    self.touch_minus.wait_for_touch(TOUCH_THRESH_MINUS),
                ),
            )
            .await
            {
                // LOGIC_INTERVAL Timer
                Either3::First(_timeout) => {
                    self.logic_updates(tick).await;
                    tick += 1;
                }
                // Overrides
                Either3::Second(control) => {
                    info!("Applying control struct {control:?}");
                    todo!()
                }
                // Touch pad pressed
                Either3::Third(control) => {
                    if tick.wrapping_sub(last_touch_tick) > 2 {
                        match control {
                            // Plus touchpad
                            Either::First(_) => {
                                let new_target = HOUSE_STATUS.lock(|s| {
                                    if s.borrow_mut().temp_target < MAX_TEMP {
                                        s.borrow_mut().temp_target += HEAT_CTRL_STEP;
                                    }
                                    s.borrow_mut().temp_target
                                });
                                let val = self.touch_plus.try_read();
                                debug!("Plus pressed - new target: {new_target}, val: {val:?}");
                            }
                            // Minus touchpad
                            Either::Second(_) => {
                                let new_target = HOUSE_STATUS.lock(|s| {
                                    if s.borrow_mut().temp_target > MIN_TEMP {
                                        s.borrow_mut().temp_target -= HEAT_CTRL_STEP;
                                    }
                                    s.borrow_mut().temp_target
                                });
                                let val = self.touch_plus.try_read();
                                debug!("Minus pressed - new target: {new_target}, val: {val:?}");
                            }
                        }
                        last_touch_tick = tick;
                        self.display_reset_ticks = 30;
                    }
                }
            }
        }
    }
}

#[embassy_executor::task]
async fn run(mut house: House) {
    house.run().await
}

fn mqtt_pub_callback() -> Option<String<1024>> {
    let status = HOUSE_STATUS.lock(|s| *s.borrow());
    // serde_json_core produces a heapless v0.7 String but we need a heapless v0.8 String, therefore the conversion
    Some(String::try_from(to_string::<_, 256>(&status).ok()?.as_str()).unwrap())
}

fn mqtt_sub_callback(topic: &str, message: &str) {
    info!("mqtt_sub_callback on topic {topic} with msg {message}");
    match from_str(message) {
        Ok((control, _nr_bytes)) => {
            if let Err(e) = CONTROL_CHANNEL.try_send(control) {
                error!("Too many control requests at once: {e:?}");
            }
        }
        Err(e) => {
            error!("invalid control JSON: {e}")
        }
    }
}

fn html_status_callback() -> String<512> {
    debug!("html status callback");
    String::try_from("1|1;1|0;0|1;0|0;1|1;1|0").unwrap()
}

#[main]
async fn main(spawner: Spawner) {
    let peripherals = Peripherals::take();
    let system = SystemControl::new(peripherals.SYSTEM);

    let clocks_initialized = ClockControl::max(system.clock_control).freeze();
    let timg0 = TimerGroup::new(peripherals.TIMG0, &clocks_initialized);
    let timer0: ErasedTimer = timg0.timer0.into();
    static TIMERS: StaticCell<[OneShotTimer<ErasedTimer>; 1]> = StaticCell::new();
    let timers = TIMERS.init([OneShotTimer::new(timer0)]);
    esp_hal_embassy::init(&clocks_initialized, timers);

    init_logger(log::LevelFilter::Debug);
    info!("Logger is setup");
    let rng = Rng::new(peripherals.RNG);
    let io = Io::new(peripherals.GPIO, peripherals.IO_MUX);
    let rtc = Rtc::new(peripherals.LPWR);

    let mut house = House::new(
        io.pins,
        peripherals.I2C0,
        &clocks_initialized,
        peripherals.RMT,
        rng,
        peripherals.ADC1,
        peripherals.DAC2,
        peripherals.SPI2,
        peripherals.DMA,
        peripherals.TOUCH,
        peripherals.MCPWM0,
        rtc,
    )
    .await;
    house.startup_animation().await;

    // === Network Configuration ===
    let ip_cfg = match CONFIG.house.ip_addr {
        "dhcp" => IpConfig::Dhcp(HOSTNAME),
        addr => IpConfig::StaticV4(str_to_ipv4_cidr(addr).expect("Invalid IP Address"), None),
    };
    let network_cfg = NetworkConfig {
        ssid: CONFIG.wifi.ssid,
        passwd: CONFIG.wifi.passwd,
        ip_cfg,
    };

    // === MQTT Configuration ===
    let (broker_ip, broker_port) =
        str_to_ipv4_port(CONFIG.mqtt.broker).expect("Invalid MQTT broker address");
    let topic_pub = MQTT_TOPIC_PUB.init({
        let mut s = String::<64>::new();
        write!(s, "{}/{CLIENT_NAME}", CONFIG.mqtt.topic).unwrap();
        s
    });
    let topic_sub = MQTT_TOPIC_SUB.init({
        let mut s = String::<64>::new();
        write!(s, "{}/{CLIENT_NAME}/cmd", CONFIG.mqtt.topic).unwrap();
        s
    });
    let mqtt_config = MQTTConfig {
        client_id: "{CLIENT_NAME}",
        broker: IpEndpoint::new(IpAddress::Ipv4(broker_ip), broker_port),
        pub_config: MQTTPublicationConfig {
            topic_pub,
            pub_interval: Duration::from_secs(1),
            callback: mqtt_pub_callback,
        },
        sub_config: Some(MQTTSubscriptionConfig {
            topic_sub,
            callback: mqtt_sub_callback,
        }),
    };

    // === HTTP Server Configuration ===
    let html_files = FlashFileReader::new(0x110000);
    let http_config = HtmlConfig {
        html_files,
        get_config: Some(HttpGetConfig {
            callback: html_status_callback,
            path: "status",
        }),
        post_config: None,
    };

    init_legos(
        &spawner,
        network_cfg,
        mqtt_config,
        http_config,
        rng,
        peripherals.WIFI,
        peripherals.TIMG1,
        &clocks_initialized,
        peripherals.RADIO_CLK,
    );

    spawner.spawn(run(house)).ok();
}
