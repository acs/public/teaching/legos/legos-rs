use esp_hal::{
    clock::Clocks,
    gpio::{Level, Output, OutputPin},
    mcpwm::{
        operator::{PwmPin, PwmPinConfig},
        timer::PwmWorkingMode,
        McPwm, PeripheralClockConfig,
    },
    peripheral::Peripheral,
    peripherals::MCPWM0,
};
use fugit::RateExtU32;
use serde::Serialize;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Serialize)]
pub enum HVACMode {
    Heat,
    Cool,
    Idle,
}

pub struct Hvac<HPin: OutputPin + 'static, CPin: OutputPin + 'static, FPin: OutputPin + 'static> {
    heat_pin: PwmPin<'static, HPin, MCPWM0, 0, true>,
    cool_pin: PwmPin<'static, CPin, MCPWM0, 1, true>,
    fan_pin: Output<'static, FPin>,
}
impl<HPin: OutputPin, CPin: OutputPin, FPin: OutputPin> Hvac<HPin, CPin, FPin> {
    pub fn new(
        heat_pin: impl Peripheral<P = HPin> + 'static,
        cool_pin: impl Peripheral<P = CPin> + 'static,
        fan_pin: impl Peripheral<P = FPin> + 'static,
        mcpwm0: MCPWM0,
        clocks: &Clocks<'static>,
    ) -> Self {
        let clock_cfg = PeripheralClockConfig::with_frequency(clocks, 32_u32.MHz()).unwrap();
        let mut mcpwm = McPwm::new(mcpwm0, clock_cfg);
        mcpwm.operator0.set_timer(&mcpwm.timer0);
        let heat_pwm = mcpwm
            .operator0
            .with_pin_a(heat_pin, PwmPinConfig::UP_ACTIVE_HIGH);

        let cool_pwm = mcpwm
            .operator1
            .with_pin_a(cool_pin, PwmPinConfig::UP_ACTIVE_HIGH);

        // start timer with timestamp values in the range of 0..=99
        let timer_clock_cfg = clock_cfg.timer_clock_with_prescaler(40, PwmWorkingMode::Increase, 1);
        mcpwm.timer0.start(timer_clock_cfg);
        let mut fan_pin = Output::new(fan_pin, Level::Low);
        fan_pin.set_high();

        Self {
            heat_pin: heat_pwm,
            cool_pin: cool_pwm,
            fan_pin,
        }
    }

    pub fn set_mode(&mut self, mode: HVACMode) {
        const PELTIER_PWM: u16 = 2;
        match mode {
            HVACMode::Heat => {
                self.heat_pin.set_timestamp(PELTIER_PWM);
                self.cool_pin.set_timestamp(0);
                self.fan_pin.set_high();
            }
            HVACMode::Cool => {
                self.heat_pin.set_timestamp(0);
                self.cool_pin.set_timestamp(PELTIER_PWM);
                self.fan_pin.set_high();
            }
            HVACMode::Idle => {
                self.heat_pin.set_timestamp(0);
                self.cool_pin.set_timestamp(0);
                self.fan_pin.set_low();
            }
        }
    }
}
