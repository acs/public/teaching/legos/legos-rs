# Cargo chef is used to download all necessary cargo crates and prebuild the binary. This can speed up the CI by a lot.
FROM docker.io/espressif/idf-rust:esp32_1.82.0.3 AS rust-esp
RUN espup install --targets=esp32
ARG ci_project_dir
WORKDIR $ci_project_dir

From rust-esp as chef
RUN rustup default stable
RUN cargo install cargo-chef
ARG ci_project_dir
WORKDIR $ci_project_dir

FROM chef AS planner
ARG ci_project_dir
WORKDIR $ci_project_dir
COPY . .
RUN cargo chef prepare --recipe-path recipe.json


FROM chef AS builder
ARG ci_project_dir
WORKDIR $ci_project_dir
COPY --from=planner --chown=esp:esp $ci_project_dir/recipe.json recipe.json
COPY --from=planner --chown=esp:esp $ci_project_dir/rust-toolchain.toml rust-toolchain.toml
COPY --from=planner --chown=esp:esp $ci_project_dir/legos/.cargo legos/.cargo
COPY --from=planner --chown=esp:esp $ci_project_dir/substation/.cargo substation/.cargo
COPY --from=planner --chown=esp:esp $ci_project_dir/house/.cargo house/.cargo
COPY --from=planner --chown=esp:esp $ci_project_dir/branch_lv/.cargo branch_lv/.cargo
COPY --from=planner --chown=esp:esp $ci_project_dir/branch_hv/.cargo branch_hv/.cargo

RUN cargo chef cook --no-std --workspace --no-build
# Build "application". Linking fails here, but that does not matter, as long as all dependencies are downloaded and built.
RUN . $HOME/export-esp.sh && \
    for d in substation legos house branch_lv branch_hv; do \
        cd $d ; \
        cargo build --release ; \
        cargo clippy --release ; \
        cd .. ; \
    done

# The actual Dockerfile used in the CI
FROM rust-esp AS runtime
ARG ci_project_dir
WORKDIR $ci_project_dir
# we copy the target folder to /ci-cache and move that to the project directory during runtime to prevent deletion through the ci git fetch/clone steps
COPY --from=builder --chown=esp:esp $ci_project_dir/target /ci-cache/target
USER root
RUN chown esp:esp -R /ci-cache
USER esp
