#!/bin/bash
#
# This script generates the files for the flash partition and flashes them if neccessary before flashing the application
#set -x
set -e

BAUD=1000000

FLASH_FILES=data.bin
MD5_FILE=.prev_flash

CUR_DIR=`pwd`
SCRIPT_PATH=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

# Build resources
cd $SCRIPT_PATH/prepare_static
echo "Generating static website files"
cargo run --release -- $CUR_DIR/$1 $CUR_DIR/$FLASH_FILES 2>&1 > /dev/null
cd $CUR_DIR

MD5=($(md5sum $CUR_DIR/$FLASH_FILES))

if [[ "$MD5" == `cat $MD5_FILE 2>/dev/null` ]]; then
    echo "No change in serialized files, no need for flashing"
else
    echo MD5 differ, flashing
    espflash write-bin --baud=$BAUD 0x110000 $CUR_DIR/$FLASH_FILES
    echo $MD5 > $MD5_FILE
fi


espflash flash --baud=$BAUD --monitor $2
