#![doc(
    html_logo_url = "../legos_logo_simple.svg",
    html_favicon_url = "../favicon.ico"
)]
//! # LEGOS high voltage Branch

#![no_std]
#![no_main]
use constcat::concat;
use core::{cell::RefCell, fmt::Write, ops::Rem};
use embassy_embedded_hal::shared_bus::asynch::i2c::I2cDevice;
use embassy_executor::Spawner;
use embassy_futures::select::{select3, Either3};
use embassy_net::{IpAddress, IpEndpoint};
use embassy_sync::{
    blocking_mutex::{raw::CriticalSectionRawMutex, Mutex as BlockingMutex},
    channel::Channel,
    mutex::Mutex,
};
use embassy_time::{Duration, Ticker, Timer};
use embedded_hal::digital::OutputPin;
use embedded_hal_async::i2c::I2c;
use esp_hal::{
    clock::{ClockControl, Clocks, CpuClock},
    gpio::{GpioPin, Io, Level, Output, Pins},
    i2c::I2C,
    peripherals::{Peripherals, I2C0},
    prelude::*,
    rng::Rng,
    system::SystemControl,
    timer::{timg::TimerGroup, ErasedTimer, OneShotTimer},
    Async,
};
use esp_println::logger::init_logger;
use heapless::String;
use legos::{
    devices::{
        ina233::{INA233Error, INA233},
        pca9956::{PCA9956Error, PCA9956},
    },
    i2c::reset_devices,
    init_legos,
    network::{str_to_ipv4_cidr, str_to_ipv4_port},
    power::PowerReading,
    FlashFileReader, HtmlConfig, HttpGetConfig, HttpPostConfig, IpConfig, MQTTConfig,
    MQTTPublicationConfig, MQTTSubscriptionConfig, NetworkConfig,
};
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use num_traits::{abs, float::FloatCore};
use serde::{Deserialize, Serialize};
use serde_json_core::{from_str, to_string};
use static_cell::StaticCell;
use static_toml::static_toml;

static_toml! {
    const CONFIG = include_toml!("../legos_cfg.toml");
}

const BRANCH_NR: usize = match option_env!("BRANCH_NR") {
    Some(s) => {
        if s.len() == 1 {
            (s.as_bytes()[0] - b'0') as usize
        } else if s.len() == 2 {
            (s.as_bytes()[0] - b'0') as usize * 10 + (s.as_bytes()[1] - b'0') as usize
        } else {
            panic!("Invalid BRANCH_NR")
        }
    }
    None => 0,
};

const HOSTNAME: &str = concat!(CONFIG.branch_hv.hostname, "-", {
    // unwrap is not const...
    match option_env!("BRANCH_NR") {
        Some(s) => s,
        None => "0",
    }
});
const CLIENT_NAME: &str = concat!("HighVoltageBranch", {
    // unwrap is not const...
    match option_env!("BRANCH_NR") {
        Some(s) => s,
        None => "0",
    }
});

static MQTT_TOPIC_PUB: StaticCell<String<64>> = StaticCell::new();
static MQTT_TOPIC_SUB: StaticCell<String<64>> = StaticCell::new();

const UPDATE_INTERVAL: Duration = Duration::from_millis(100); // Update interval for readings in BranchHvState
const UPDATE_INTERVAL_ANIMATION: Duration = Duration::from_millis(20); // Update interval for the powerflow animation

static UNDER_CURRENT_LIMIT: f32 = CONFIG.branch_hv.undercurrent_limit as f32;
static OVER_CURRENT_LIMIT: f32 = CONFIG.branch_hv.overcurrent_limit as f32;
static UNDER_VOLTAGE_LIMIT: f32 = CONFIG.branch_hv.undervoltage_limit as f32;
static OVER_VOLTAGE_LIMIT: f32 = CONFIG.branch_hv.overvoltage_limit as f32;

#[derive(Debug, Copy, Clone)]
pub enum BranchError {
    I2CError,
    INAAError(INA233Error),
    INABError(INA233Error),
    PCAAError(PCA9956Error),
    PCABError(PCA9956Error),
    InvalidDevice,
    InvalidArgument,
}

#[derive(Debug, Copy, Clone, Serialize)]
pub enum OperationStatus {
    Normal,
    Open,
    Short,
}

#[derive(Debug, Copy, Clone, Serialize)]
pub struct FaultStatus {
    undercurrent: bool,
    overcurrent: bool,
    undervoltage: bool,
    overvoltage: bool,
}

#[derive(Debug, Copy, Clone, Serialize)]
pub enum CurrentFlow {
    AtoB,
    AtoShort,
    BtoA,
    BtoShort,
    None,
}

#[derive(Debug, Copy, Clone, Deserialize)]
pub enum OverrideType {
    Open,
    Short,
}

#[derive(Debug, Copy, Clone, Serialize)]
/// Current state of the high voltage branch. Used for control algorithms and external
/// reporting (MQTT/HTTP).
struct BranchHvState {
    power_a: PowerReading,
    power_b: PowerReading,
    operation_status: OperationStatus,
    fault_status: FaultStatus,
    current_flow: CurrentFlow,
    override_active: bool,
    burned: bool,
}

impl BranchHvState {
    const fn new() -> Self {
        Self {
            power_a: PowerReading {
                voltage: 0.0,
                current: 0.0,
                power: 0.0,
            },
            power_b: PowerReading {
                voltage: 0.0,
                current: 0.0,
                power: 0.0,
            },
            operation_status: OperationStatus::Normal,
            fault_status: FaultStatus {
                undercurrent: false,
                overcurrent: false,
                undervoltage: false,
                overvoltage: false,
            },
            current_flow: CurrentFlow::None,
            override_active: false,
            burned: false,
        }
    }
}

#[derive(Debug, Copy, Clone, Deserialize)]
/// Datastructure for overriding control in [`BranchHv`]. Generated externally
/// (MQTT/HTTP)
struct BranchHvExternalControl {
    override_type: OverrideType,
}

static BRANCHHV_STATUS: BlockingMutex<CriticalSectionRawMutex, RefCell<BranchHvState>> =
    BlockingMutex::new(RefCell::new(BranchHvState::new()));
static I2C_BUS: StaticCell<Mutex<CriticalSectionRawMutex, I2C<'_, I2C0, Async>>> =
    StaticCell::new();
static CONTROL_CHANNEL: Channel<CriticalSectionRawMutex, BranchHvExternalControl, 4> =
    Channel::new();

/// Struct to control the rows of LEDs
struct LedControl<I2CA: I2c, I2CB: I2c, PIN: OutputPin> {
    pca9956_a: PCA9956<I2CA, PIN>,
    pca9956_b: PCA9956<I2CB, PIN>,
    // Pin arrays
    a_top: [usize; 10],
    a_bot: [usize; 10],
    b_top: [usize; 10],
    b_bot: [usize; 10],
}

impl<I2CA: I2c, I2CB: I2c, PIN: OutputPin> LedControl<I2CA, I2CB, PIN> {
    async fn new(i2c_a: I2CA, i2c_b: I2CB, mut not_oe_pin: PIN) -> Result<Self, BranchError> {
        // Set max current in mA for channels that get used (not used channels have to get 0.0)
        let mut i_max_ma_array_a = [5.0; 24];
        i_max_ma_array_a[4] = 0.0;
        i_max_ma_array_a[11] = 0.0;
        i_max_ma_array_a[12] = 0.0;
        i_max_ma_array_a[17] = 0.0;
        let mut i_max_ma_array_b = [5.0; 24];
        i_max_ma_array_b[5] = 0.0;
        i_max_ma_array_b[11] = 0.0;
        i_max_ma_array_b[12] = 0.0;
        i_max_ma_array_b[17] = 0.0;

        info!("Initializing pca9956_a");
        let pca9956_a = PCA9956::new(i2c_a, 0x15, 2000.0, i_max_ma_array_a, None)
            .await
            .map_err(BranchError::PCAAError)?;
        info!("Initializing pca9956_b");
        let pca9956_b = PCA9956::new(i2c_b, 0x19, 2000.0, i_max_ma_array_b, None)
            .await
            .map_err(BranchError::PCABError)?;
        not_oe_pin.set_low().unwrap();

        Ok(Self {
            pca9956_a,
            pca9956_b,
            // LED Pins are arranged from left to right
            a_top: [13, 14, 15, 16, 18, 19, 20, 21, 22, 23],
            a_bot: [10, 9, 8, 7, 6, 5, 3, 2, 1, 0],
            b_top: [13, 14, 15, 16, 18, 19, 20, 21, 22, 23],
            b_bot: [10, 9, 8, 7, 6, 4, 3, 2, 1, 0],
        })
    }

    async fn startup_animation(&mut self) -> Result<(), BranchError> {
        for i in 0..10 {
            let mut all_duties_a: [u8; 24] = [0; 24];
            let mut all_duties_b: [u8; 24] = [0; 24];
            all_duties_a[self.a_top[i]] = 80;
            all_duties_b[self.b_bot[9 - i]] = 80;
            self.pca9956_a
                .set_leds(&all_duties_a)
                .await
                .map_err(BranchError::PCAAError)?;
            self.pca9956_b
                .set_leds(&all_duties_b)
                .await
                .map_err(BranchError::PCABError)?;
            Timer::after(Duration::from_millis(50)).await;
        }
        for i in 0..10 {
            let mut all_duties_a: [u8; 24] = [0; 24];
            let mut all_duties_b: [u8; 24] = [0; 24];
            all_duties_a[self.a_bot[9 - i]] = 80;
            all_duties_b[self.b_top[i]] = 80;
            self.pca9956_a
                .set_leds(&all_duties_a)
                .await
                .map_err(BranchError::PCAAError)?;
            self.pca9956_b
                .set_leds(&all_duties_b)
                .await
                .map_err(BranchError::PCABError)?;
            Timer::after(Duration::from_millis(50)).await;
        }
        Ok(())
    }

    /// Activates all LEDs with given duty
    #[allow(dead_code)]
    async fn led_all_on(&mut self, duty: u8) -> Result<(), BranchError> {
        let duties: [u8; 24] = [duty; 24];
        self.pca9956_a
            .set_leds(&duties)
            .await
            .map_err(BranchError::PCAAError)?;
        self.pca9956_b
            .set_leds(&duties)
            .await
            .map_err(BranchError::PCABError)?;
        Ok(())
    }

    /// Writes values (dependent on time variable t and current flow direction) in array "duties", which are interpolated by the function (x-1)²
    /// Displays traces on branch by activating according LEDs with values from "duties"
    /// If there is a short, only the LEDs on the corresponding half are activated
    async fn flow_animation(&mut self, t: f32, max_duty: u8) -> Result<(), BranchError> {
        let mut duties: [u8; 10] = [0; 10];
        let f = [
            (t + 0.9).rem(1.0),
            (t + 0.8).rem(1.0),
            (t + 0.7).rem(1.0),
            (t + 0.6).rem(1.0),
            (t + 0.5).rem(1.0),
            (t + 0.4).rem(1.0),
            (t + 0.3).rem(1.0),
            (t + 0.2).rem(1.0),
            (t + 0.1).rem(1.0),
            t.rem(1.0),
        ];

        // Get flow_status
        let mut flow_status = CurrentFlow::None;
        BRANCHHV_STATUS.lock(|s| {
            flow_status = s.borrow().current_flow;
        });

        let mut all_duties_a: [u8; 24] = [0; 24];
        let mut all_duties_b: [u8; 24] = [0; 24];

        // Set animation direction
        match flow_status {
            CurrentFlow::AtoB | CurrentFlow::AtoShort => {
                duties.iter_mut().enumerate().for_each(|(i, d)| {
                    *d = ((f[i] - 1.0) * (f[i] - 1.0) * (max_duty as f32)) as u8
                });
            }
            CurrentFlow::BtoA | CurrentFlow::BtoShort => {
                duties.iter_mut().rev().enumerate().for_each(|(i, d)| {
                    *d = ((f[i] - 1.0) * (f[i] - 1.0) * (max_duty as f32)) as u8
                });
            }
            CurrentFlow::None => {
                all_duties_a = [0; 24];
                all_duties_b = [0; 24];
            }
        }

        // Write duties in correct positions
        match flow_status {
            CurrentFlow::AtoB | CurrentFlow::BtoA => {
                for i in 0..10 {
                    all_duties_a[self.a_bot[i]] = duties[i];
                    all_duties_a[self.a_top[i]] = duties[(i + 5) % 10];
                    all_duties_b[self.b_bot[i]] = duties[i];
                    all_duties_b[self.b_top[i]] = duties[(i + 5) % 10];
                }
            }
            CurrentFlow::AtoShort => {
                for i in 0..10 {
                    all_duties_a[self.a_bot[i]] = duties[i];
                    all_duties_a[self.a_top[i]] = duties[(i + 5) % 10];
                }
            }
            CurrentFlow::BtoShort => {
                for i in 0..10 {
                    all_duties_b[self.b_bot[i]] = duties[i];
                    all_duties_b[self.b_top[i]] = duties[(i + 5) % 10];
                }
            }
            CurrentFlow::None => {}
        }

        self.pca9956_a
            .set_leds(&all_duties_a)
            .await
            .map_err(BranchError::PCAAError)?;
        self.pca9956_b
            .set_leds(&all_duties_b)
            .await
            .map_err(BranchError::PCABError)?;
        Ok(())
    }
}

/// Holds the handles to the high voltage branch hardware
struct BranchHv {
    ina233_a: INA233<I2cDevice<'static, CriticalSectionRawMutex, I2C<'static, I2C0, Async>>>,
    ina233_b: INA233<I2cDevice<'static, CriticalSectionRawMutex, I2C<'static, I2C0, Async>>>,
    #[allow(clippy::type_complexity)]
    led_control: LedControl<
        I2cDevice<'static, CriticalSectionRawMutex, I2C<'static, I2C0, Async>>,
        I2cDevice<'static, CriticalSectionRawMutex, I2C<'static, I2C0, Async>>,
        Output<'static, GpioPin<22>>,
    >,
    en_g_pin: Output<'static, GpioPin<2>>, // Pin to enable short (ties bound to ground) (high = short)
    not_en_s_pin: Output<'static, GpioPin<21>>, // Pin to disable powerflow over bus (high = powerflow disabled)
    override_state: Option<Duration>,
}

impl BranchHv {
    async fn new(pins: Pins, i2c: I2C0, clocks: &Clocks<'static>) -> Result<Self, BranchError> {
        info!("Starting I2C");
        let sda = pins.gpio4;
        let scl = pins.gpio0;
        let mut i2c_bus = I2C::new_async(i2c, sda, scl, 400.kHz(), clocks);
        reset_devices(&mut i2c_bus).await;
        //bus_map(&mut i2c_bus).await;
        let i2c_bus_mutex = Mutex::<CriticalSectionRawMutex, _>::new(i2c_bus);
        let i2c_bus_static = I2C_BUS.init(i2c_bus_mutex);

        let mut ina233_a = INA233::new(I2cDevice::new(i2c_bus_static), 0x40, 0.010, 8.0)
            .await
            .map_err(BranchError::INAAError)?;

        ina233_a
            .set_calibration(
                CONFIG.branch_hv.voltage_offsets[BRANCH_NR] as f32,
                CONFIG.branch_hv.current_offsets[BRANCH_NR] as f32,
            )
            .await
            .map_err(BranchError::INAAError)?;

        ina233_a
            .set_alert(OVER_CURRENT_LIMIT, OVER_VOLTAGE_LIMIT)
            .await
            .map_err(BranchError::INAAError)?;

        let mut ina233_b = INA233::new(I2cDevice::new(i2c_bus_static), 0x41, 0.010, 8.0)
            .await
            .map_err(BranchError::INABError)?;

        ina233_b
            .set_calibration(
                CONFIG.branch_hv.voltage_offsets[BRANCH_NR] as f32,
                CONFIG.branch_hv.current_offsets[BRANCH_NR] as f32,
            )
            .await
            .map_err(BranchError::INAAError)?;

        ina233_b
            .set_alert(OVER_CURRENT_LIMIT, OVER_VOLTAGE_LIMIT)
            .await
            .map_err(BranchError::INABError)?;

        Ok(Self {
            ina233_a,
            ina233_b,
            led_control: LedControl::new(
                I2cDevice::new(i2c_bus_static),
                I2cDevice::new(i2c_bus_static),
                Output::new(pins.gpio22, Level::High),
            )
            .await?,
            en_g_pin: Output::new(pins.gpio2, Level::Low),
            not_en_s_pin: Output::new(pins.gpio21, Level::Low),
            override_state: None,
        })
    }

    async fn startup_animation(&mut self) -> Result<(), BranchError> {
        self.led_control.startup_animation().await
    }

    async fn update_status(&mut self) -> Result<f32, BranchError> {
        let mut current_a: f32 = 0.0;
        let mut current_b: f32 = 0.0;
        let mut voltage_a: f32 = 0.0;
        let mut voltage_b: f32 = 0.0;

        // Update PowerReading power_a
        if let Ok(pwr_a) = self.ina233_a.read_power().await {
            current_a = pwr_a.current;
            voltage_a = pwr_a.voltage;
            BRANCHHV_STATUS.lock(|s| {
                s.borrow_mut().power_a = pwr_a;
            });
        } else {
            error!("Reading from INA A failed");
        }

        // Update PowerReading power_b
        if let Ok(pwr_b) = self.ina233_b.read_power().await {
            current_b = pwr_b.current;
            voltage_b = pwr_b.voltage;
            BRANCHHV_STATUS.lock(|s| {
                s.borrow_mut().power_b = pwr_b;
            });
        } else {
            error!("Reading from INA B failed");
        }

        // Clear Ina alerts
        if voltage_a.abs() < OVER_VOLTAGE_LIMIT && current_a.abs() < OVER_CURRENT_LIMIT {
            self.ina233_a
                .clear_alert()
                .await
                .map_err(BranchError::INAAError)?;
        }
        if voltage_b.abs() < OVER_VOLTAGE_LIMIT && current_b.abs() < OVER_CURRENT_LIMIT {
            self.ina233_b
                .clear_alert()
                .await
                .map_err(BranchError::INABError)?;
        }

        // Update override_active and fault_status
        BRANCHHV_STATUS.lock(|s| {
            s.borrow_mut().override_active = self.override_state.is_some();
            s.borrow_mut().fault_status.undercurrent =
                current_a.abs() < UNDER_CURRENT_LIMIT && current_b.abs() < UNDER_CURRENT_LIMIT;
            s.borrow_mut().fault_status.overcurrent =
                current_a.abs() > OVER_CURRENT_LIMIT || current_b.abs() > OVER_CURRENT_LIMIT;
            s.borrow_mut().fault_status.undervoltage =
                voltage_a.abs() < UNDER_VOLTAGE_LIMIT && voltage_b.abs() < UNDER_VOLTAGE_LIMIT;
            s.borrow_mut().fault_status.overvoltage =
                voltage_a.abs() > OVER_VOLTAGE_LIMIT || voltage_b.abs() > OVER_VOLTAGE_LIMIT;
        });

        // Update operation_status and current_flow
        if current_a < -0.005 && current_b > 0.005 {
            BRANCHHV_STATUS.lock(|s| {
                s.borrow_mut().current_flow = CurrentFlow::AtoB;
                s.borrow_mut().operation_status = OperationStatus::Normal;
            });
            Ok(abs(current_a))
        } else if current_a > 0.005 && current_b < -0.005 {
            BRANCHHV_STATUS.lock(|s| {
                s.borrow_mut().current_flow = CurrentFlow::BtoA;
                s.borrow_mut().operation_status = OperationStatus::Normal;
            });
            Ok(abs(current_b))
        } else if abs(current_a) < 0.005 && abs(current_b) < 0.005 {
            BRANCHHV_STATUS.lock(|s| {
                s.borrow_mut().current_flow = CurrentFlow::None;
                s.borrow_mut().operation_status = OperationStatus::Open;
            });
            Ok(0.0)
        } else if current_a < -0.005 && abs(current_b) < 0.005 {
            BRANCHHV_STATUS.lock(|s| {
                s.borrow_mut().current_flow = CurrentFlow::AtoShort;
                s.borrow_mut().operation_status = OperationStatus::Short;
            });
            Ok(abs(current_a))
        } else if abs(current_a) < 0.005 && current_b < -0.005 {
            BRANCHHV_STATUS.lock(|s| {
                s.borrow_mut().current_flow = CurrentFlow::BtoShort;
                s.borrow_mut().operation_status = OperationStatus::Short;
            });
            Ok(abs(current_b))
        } else {
            Ok(0.0)
        }
    }

    #[allow(dead_code)]
    fn print_status(&self) {
        let s = BRANCHHV_STATUS.lock(|s| *s.borrow());
        debug!("{s:#?}");
    }

    fn apply_external_control(&mut self, control: &BranchHvExternalControl) {
        info!("Applying override");

        self.override_state = Some(Duration::from_secs(CONFIG.general.override_duration as u64));

        match control.override_type {
            OverrideType::Open => {
                self.en_g_pin.set_low();
                self.not_en_s_pin.set_high();
            }
            OverrideType::Short => {
                self.not_en_s_pin.set_low();
                self.en_g_pin.set_high();
            }
        }
    }

    fn end_external_control(&mut self) {
        info!("Ending override");
        self.en_g_pin.set_low();
        self.not_en_s_pin.set_low();
        self.override_state = None;
    }

    pub async fn run(&mut self) {
        let mut ticker_update = Ticker::every(UPDATE_INTERVAL);
        let mut ticker_animation = Ticker::every(UPDATE_INTERVAL_ANIMATION);
        let mut t_animation: f32 = 0.0;
        let mut time_overcurrent = Duration::from_secs(0);
        let mut time_disabled = Duration::from_secs(0);
        let mut blink_stepper: i32 = 0;
        let mut current: f32 = 0.0;
        let mut i: i32 = 0;
        loop {
            match select3(
                ticker_update.next(),
                ticker_animation.next(),
                CONTROL_CHANNEL.receive(),
            )
            .await
            {
                Either3::First(_timeout) => {
                    if let Some(duration) = self.override_state.as_mut() {
                        if *duration >= UPDATE_INTERVAL {
                            *duration -= UPDATE_INTERVAL;
                        } else {
                            self.end_external_control();
                            Timer::after(Duration::from_millis(1)).await;
                        }
                    }
                    current = self.update_status().await.unwrap();

                    // If the branch is in overcurrent for more than 5 seconds, it "burns through" (blinks three times and disables for the next 20 seconds)
                    if BRANCHHV_STATUS.lock(|s| s.borrow().fault_status.overcurrent) {
                        time_overcurrent += UPDATE_INTERVAL;
                    } else {
                        time_overcurrent = Duration::from_secs(0);
                    }

                    if time_disabled > Duration::from_secs(0) {
                        time_disabled -= UPDATE_INTERVAL;
                        if time_disabled <= Duration::from_secs(0) {
                            time_disabled = Duration::from_secs(0);
                            blink_stepper = 0;
                            BRANCHHV_STATUS.lock(|s| s.borrow_mut().burned = false);
                            self.not_en_s_pin.set_low();
                        } else {
                            match blink_stepper {
                                0 | 6 | 12 | 18 => self.led_control.led_all_on(0).await.unwrap(),
                                3 | 9 | 15 => self.led_control.led_all_on(80).await.unwrap(),
                                _ => {}
                            }
                            blink_stepper += 1;
                        }
                    }

                    if time_overcurrent > Duration::from_secs(5) {
                        time_disabled = Duration::from_secs(20);
                        if let Some(_duration) = self.override_state.as_mut() {
                            info!("Ending override due to burned through branch");
                            self.end_external_control();
                        }
                        BRANCHHV_STATUS.lock(|s| s.borrow_mut().burned = true);
                        self.not_en_s_pin.set_high();
                    }

                    i = i.wrapping_add(1);
                    if i % 20 == 0 {
                        self.print_status();
                    }
                }
                Either3::Second(_timeout) => {
                    if time_disabled == Duration::from_secs(0) {
                        self.led_control
                            .flow_animation(t_animation, 80)
                            .await
                            .unwrap();
                        t_animation += 0.1 * current;
                        if t_animation > 1.0 {
                            t_animation -= 1.0;
                        }
                    }
                }
                Either3::Third(control) => {
                    if BRANCHHV_STATUS.lock(|s| s.borrow().burned) {
                        info!("Override not possible due to burned through branch");
                    } else {
                        info!("Applying control struct {control:?}");
                        self.apply_external_control(&control);
                        Timer::after(Duration::from_millis(1)).await;
                        self.update_status().await.unwrap();
                    }
                }
            }
        }
    }
}

#[embassy_executor::task]
async fn run(mut branchhv: BranchHv) {
    branchhv.run().await
}

fn mqtt_pub_callback() -> Option<String<1024>> {
    let status = BRANCHHV_STATUS.lock(|s| *s.borrow());
    // serde_json_core produces a heapless v0.7 String but we need a heapless v0.8 String, therefore the conversion
    Some(String::try_from(to_string::<_, 1024>(&status).ok()?.as_str()).unwrap())
}

fn mqtt_sub_callback(topic: &str, message: &str) {
    info!("mqtt_sub_callback on topic {topic} with msg {message}");
    match from_str(message) {
        Ok((control, _nr_bytes)) => {
            if let Err(e) = CONTROL_CHANNEL.try_send(control) {
                error!("Too many control requests at once: {e:?}");
            }
        }
        Err(e) => {
            error!("invalid control JSON: {e}")
        }
    }
}

fn html_status_callback() -> String<512> {
    trace!("html status callback");
    let status = BRANCHHV_STATUS.lock(|s| *s.borrow());
    // serde_json_core produces a heapless v0.7 String but we need a heapless v0.8 String, therefore the conversion
    String::try_from(to_string::<_, 512>(&status).ok().unwrap().as_str()).unwrap()
}

fn html_override_callback(s: String<512>) {
    info!("html override: {s}");
    match from_str(s.as_str()) {
        Ok((control, _nr_bytes)) => {
            if let Err(e) = CONTROL_CHANNEL.try_send(control) {
                error!("Too many control requests at once: {e:?}");
            }
        }
        Err(e) => {
            error!("invalid control JSON: {e}")
        }
    }
}

#[main]
async fn main(spawner: Spawner) {
    let peripherals = Peripherals::take();
    let system = SystemControl::new(peripherals.SYSTEM);

    let clocks_initialized =
        ClockControl::configure(system.clock_control, CpuClock::Clock80MHz).freeze();
    let timg0 = TimerGroup::new(peripherals.TIMG0, &clocks_initialized);
    let timer0: ErasedTimer = timg0.timer0.into();
    static TIMERS: StaticCell<[OneShotTimer<ErasedTimer>; 1]> = StaticCell::new();
    let timers = TIMERS.init([OneShotTimer::new(timer0)]);
    esp_hal_embassy::init(&clocks_initialized, timers);

    init_logger(log::LevelFilter::Debug);
    info!("Logger is setup");

    let rng = Rng::new(peripherals.RNG);

    let io = Io::new(peripherals.GPIO, peripherals.IO_MUX);
    let mut branchhv = BranchHv::new(io.pins, peripherals.I2C0, &clocks_initialized)
        .await
        .unwrap();
    branchhv.startup_animation().await.unwrap();

    // === Network Configuration ===
    let ip_cfg = match CONFIG.branch_hv.ip_addr {
        "dhcp" => IpConfig::Dhcp(HOSTNAME),
        addr => IpConfig::StaticV4(str_to_ipv4_cidr(addr).expect("Invalid IP Address"), None),
    };
    let network_cfg = NetworkConfig {
        ssid: CONFIG.wifi.ssid,
        passwd: CONFIG.wifi.passwd,
        ip_cfg,
    };

    // === MQTT Configuration ===
    let (broker_ip, broker_port) =
        str_to_ipv4_port(CONFIG.mqtt.broker).expect("Invalid MQTT broker address");
    let topic_pub = MQTT_TOPIC_PUB.init({
        let mut s = String::<64>::new();
        write!(s, "{}/{CLIENT_NAME}", CONFIG.mqtt.topic).unwrap();
        s
    });
    let topic_sub = MQTT_TOPIC_SUB.init({
        let mut s = String::<64>::new();
        write!(s, "{}/{CLIENT_NAME}/cmd", CONFIG.mqtt.topic).unwrap();
        s
    });
    let mqtt_config = MQTTConfig {
        client_id: CLIENT_NAME,
        broker: IpEndpoint::new(IpAddress::Ipv4(broker_ip), broker_port),
        pub_config: MQTTPublicationConfig {
            topic_pub,
            pub_interval: Duration::from_secs(1),
            callback: mqtt_pub_callback,
        },
        sub_config: Some(MQTTSubscriptionConfig {
            topic_sub,
            callback: mqtt_sub_callback,
        }),
    };

    // === HTTP Server Configuration ===
    let html_files = FlashFileReader::new(0x110000);
    let http_config = HtmlConfig {
        html_files,
        get_config: Some(HttpGetConfig {
            callback: html_status_callback,
            path: "status",
        }),
        post_config: Some(HttpPostConfig {
            callback: html_override_callback,
            path: "override",
        }),
    };

    // === Start the tasks ===
    init_legos(
        &spawner,
        network_cfg,
        mqtt_config,
        http_config,
        rng,
        peripherals.WIFI,
        peripherals.TIMG1,
        &clocks_initialized,
        peripherals.RADIO_CLK,
    );
    spawner.spawn(run(branchhv)).ok();
}
