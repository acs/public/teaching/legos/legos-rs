use serde::Serialize;

/// Readings from a power sensor.
#[derive(Debug, Copy, Clone, Default, Serialize)]
pub struct PowerReading {
    /// In Volts (V)
    pub voltage: f32,
    /// In Ampere (A)
    pub current: f32,
    /// In Watt (W)
    pub power: f32,
}
