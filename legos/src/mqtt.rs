use embassy_futures::select::{select3, Either3};
use embassy_net::{tcp::TcpSocket, IpEndpoint, Stack};
use embassy_time::{Duration, Timer};
use esp_hal::rng::Rng;
use esp_wifi::wifi::{WifiDevice, WifiStaDevice};
use heapless::String;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use rust_mqtt::client::client_config::MqttVersion::MQTTv5;
use rust_mqtt::client::{client::MqttClient, client_config::ClientConfig};

/// Is called regularly by the MQTT task. When a MQTT publication on the topic
/// specified in the config is desired, return `Some(message)`. `None` skips
/// publication.
pub type MqttPubCallback = fn() -> Option<String<1024>>;

/// This function is called by the MQTT task when a message on the given topic occurs. Looks like `fn sub_cb(topic: &str, message: &str)`.
pub type MqttSubCallback = fn(&str, &str);

#[derive(Debug, Copy, Clone)]
pub struct MQTTSubscriptionConfig<'a> {
    pub topic_sub: &'a str,
    pub callback: MqttSubCallback,
}

#[derive(Debug, Copy, Clone)]
pub struct MQTTPublicationConfig<'a> {
    pub topic_pub: &'a str,
    pub pub_interval: Duration,
    pub callback: MqttPubCallback,
}

#[derive(Debug, Copy, Clone)]
pub struct MQTTConfig<'a> {
    pub client_id: &'a str,
    pub broker: IpEndpoint,
    pub pub_config: MQTTPublicationConfig<'a>,
    pub sub_config: Option<MQTTSubscriptionConfig<'a>>,
}

#[embassy_executor::task]
// MQTT Client task.
// Publishes data at a regular interval and calls a callback when a message on the given subscription topic is published.
pub(crate) async fn mqtt_client_task(
    stack: &'static Stack<WifiDevice<'static, WifiStaDevice>>,
    config: MQTTConfig<'static>,
    rng: Rng,
) {
    // Regular keepalive ping to mqtt broker
    let ping_interval = Duration::from_secs(5);

    let mut rx_buffer = [0; 1536];
    let mut tx_buffer = [0; 500];
    let mut recv_buffer = [0; 500];
    let mut write_buffer = [0; 400];

    loop {
        loop {
            if stack.is_link_up() {
                break;
            }
            Timer::after(Duration::from_millis(500)).await;
        }

        let mut socket = TcpSocket::new(stack, &mut rx_buffer, &mut tx_buffer);

        socket.set_timeout(Some(embassy_time::Duration::from_secs(10)));

        info!("MQTT connecting to {:?}...", config.broker);
        let r = socket.connect(config.broker).await;
        if let Err(e) = r {
            error!("MQTT socket connect error: {:?}", e);
            Timer::after(Duration::from_millis(5000)).await;
            continue;
        }
        info!("MQTT socket connected!");

        let mut mqtt_config = ClientConfig::new(MQTTv5, rng);
        mqtt_config.add_client_id(config.client_id);
        let mut mqtt_client = MqttClient::<_, 20, _>::new(
            socket,
            &mut write_buffer,
            500,
            &mut recv_buffer,
            500,
            mqtt_config,
        );

        if let Err(e) = mqtt_client.connect_to_broker().await {
            error!("MQTT broker connection error: {:?}", e);
            Timer::after(Duration::from_millis(5000)).await;
            continue;
        }
        debug!("MQTT broker connected...");

        if let Some(sub_config) = config.sub_config {
            if let Err(e) = mqtt_client.subscribe_to_topic(sub_config.topic_sub).await {
                error!("MQTT broker connection error: {:?}", e);
                Timer::after(Duration::from_millis(5000)).await;
                continue;
            }
            info!("MQTT: Subscribed to {}", sub_config.topic_sub);
        }

        loop {
            match select3(
                mqtt_client.receive_message(),
                Timer::after(ping_interval),
                Timer::after(config.pub_config.pub_interval),
            )
            .await
            {
                // Subscription Call
                Either3::First(msg) => {
                    match msg {
                        Ok((topic, message)) => {
                            // We can only receive messages if we have subscribed
                            let sub_cfg = config.sub_config.unwrap();
                            assert_eq!(sub_cfg.topic_sub, topic);
                            let message_payload = core::str::from_utf8(message).unwrap();
                            debug!("received mqtt msg on topic {}: {}", topic, message_payload);
                            (sub_cfg.callback)(topic, message_payload);
                        }
                        Err(e) => {
                            error!("MQTT subscription error: {e:?}");
                            Timer::after(Duration::from_millis(2000)).await;
                            break;
                        }
                    }
                }
                // Ping
                Either3::Second(_timeout) => {
                    trace!("sending ping");
                    if let Err(e) = mqtt_client.send_ping().await {
                        error!("Error while sending MQTT Ping: {e}");
                        break;
                    }
                }
                // Publish
                Either3::Third(_timeout) => {
                    debug!("MQTT publish");
                    if let Some(message) = (config.pub_config.callback)() {
                        debug!(
                            "MQTT publishing on topic {}: {message}",
                            config.pub_config.topic_pub
                        );
                        if let Err(e) = mqtt_client
                            .send_message(
                                config.pub_config.topic_pub,
                                message.as_bytes(),
                                rust_mqtt::packet::v5::publish_packet::QualityOfService::QoS0,
                                false,
                            )
                            .await
                        {
                            error!("MQTT send error {e:?}");
                            break;
                        }
                        debug!("MQTT message sent...");
                    }
                }
            }
        }
    }
}
