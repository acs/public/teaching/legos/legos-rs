use crate::WEB_TASK_POOL_SIZE;
use core::fmt::Write;
use embassy_net::{
    udp::{PacketMetadata, UdpSocket},
    Config, DhcpConfig, IpAddress, IpEndpoint, Ipv4Address, Ipv4Cidr, Stack, StackResources,
    StaticConfigV4,
};
use embassy_time::{Duration, Timer};
use esp_wifi::wifi::{
    ClientConfiguration, Configuration, WifiController, WifiDevice, WifiEvent, WifiStaDevice,
    WifiState,
};
use heapless::String;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use smoltcp::wire::DnsFlags;
use static_cell::StaticCell;

/// Converts an ip_str like "123.45.67.89" to an [`Ipv4Address`].
pub fn str_to_ipv4(ip_str: &str) -> Option<Ipv4Address> {
    let mut parts = ip_str.split('.');
    Some(Ipv4Address::new(
        parts.next()?.parse().ok()?,
        parts.next()?.parse().ok()?,
        parts.next()?.parse().ok()?,
        parts.next()?.parse().ok()?,
    ))
}

/// Converts an ip_str like "123.45.67.89/24" to an [`Ipv4Cidr`].
pub fn str_to_ipv4_cidr(ip_str: &str) -> Option<Ipv4Cidr> {
    let mut parts = ip_str.split('/');
    Some(Ipv4Cidr::new(
        str_to_ipv4(parts.next()?)?,
        parts.next()?.parse().ok()?,
    ))
}

/// Converts an ip_str like "123.45.67.89:1234" to an [`Ipv4Address`] and the port.
pub fn str_to_ipv4_port(ip_str: &str) -> Option<(Ipv4Address, u16)> {
    let mut parts = ip_str.split(':');
    Some((str_to_ipv4(parts.next()?)?, parts.next()?.parse().ok()?))
}

#[derive(Debug, Clone)]
pub enum IpConfig {
    /// Static IPv4 Address and optional gateway
    StaticV4(Ipv4Cidr, Option<Ipv4Address>),
    Dhcp(&'static str),
}

#[derive(Debug, Clone)]
pub struct NetworkConfig<'a> {
    pub ssid: &'a str,
    pub passwd: &'a str,
    pub ip_cfg: IpConfig,
}

const NET_STACK_SIZE: usize = WEB_TASK_POOL_SIZE + 3;

static NET_STACK_RES: StaticCell<StackResources<NET_STACK_SIZE>> = StaticCell::new();
static NET_STACK: StaticCell<embassy_net::Stack<WifiDevice<WifiStaDevice>>> = StaticCell::new();

pub(crate) fn init_network_stack(
    wifi_interface: WifiDevice<'static, WifiStaDevice>,
    network_cfg: &NetworkConfig,
    seed: u64,
) -> &'static Stack<WifiDevice<'static, WifiStaDevice>> {
    let config = match network_cfg.ip_cfg {
        IpConfig::StaticV4(address, gateway) => Config::ipv4_static(StaticConfigV4 {
            address,
            gateway,
            dns_servers: Default::default(),
        }),
        IpConfig::Dhcp(hostname) => {
            let mut cfg = DhcpConfig::default();
            cfg.hostname = Some(String::try_from(hostname).unwrap());
            Config::dhcpv4(cfg)
        }
    };
    let stack_resources = NET_STACK_RES.init(StackResources::<NET_STACK_SIZE>::new());
    &*NET_STACK.init(Stack::new(wifi_interface, config, stack_resources, seed))
}

#[embassy_executor::task]
/// This task maintains the WiFi connection and reconnects if the connection is lost
pub(crate) async fn wifi_connection_task(
    mut controller: WifiController<'static>,
    ssid: &'static str,
    password: &'static str,
) {
    info!("Starting WiFi connection management task");
    info!("Device capabilities: {:?}", controller.get_capabilities());
    loop {
        if esp_wifi::wifi::get_wifi_state() == WifiState::StaConnected {
            // Good! Remain in this state until we're no longer connected
            controller.wait_for_event(WifiEvent::StaDisconnected).await;
            error!("WiFi disconnected");
            Timer::after(Duration::from_millis(5000)).await
        }
        if !matches!(controller.is_started(), Ok(true)) {
            let client_config = Configuration::Client(ClientConfiguration {
                ssid: ssid.try_into().unwrap(),
                password: password.try_into().unwrap(),
                ..Default::default()
            });
            controller.set_configuration(&client_config).unwrap();
            info!("Starting wifi");
            controller.start().await.unwrap();
            info!("Wifi started!");
        }
        debug!("About to connect...");

        match controller.connect().await {
            Ok(_) => info!("Wifi connected: {:?}", controller.get_configuration()),
            Err(e) => {
                error!("Failed to connect to wifi: {e:?}");
                Timer::after(Duration::from_millis(5000)).await
            }
        }
    }
}

#[embassy_executor::task]
/// Regular background task for the network stack
pub(crate) async fn net_stack_task(stack: &'static Stack<WifiDevice<'static, WifiStaDevice>>) {
    stack.run().await
}

#[embassy_executor::task]
/// Regular background task for the network stack
pub(crate) async fn net_config_print(stack: &'static Stack<WifiDevice<'static, WifiStaDevice>>) {
    let mut network_cfg = None;
    loop {
        stack.wait_config_up().await;
        let new_nw_cfg = stack.config_v4();
        if new_nw_cfg != network_cfg {
            // TODO: nice Print
            if let Some(cfg) = stack.config_v4() {
                info!("Network Configuration:");
                info!(" - IP Address: {:?}", cfg.address);
                info!(" - Gateway: {:?}", cfg.gateway);
                info!(" - DNS: {:?}", cfg.dns_servers);
            }
            network_cfg = new_nw_cfg;
        }
        Timer::after(Duration::from_millis(500)).await;
    }
}

fn dns_names_to_string<'a>(
    mut names: impl Iterator<Item = Result<&'a [u8], smoltcp::wire::Error>>,
) -> Option<(String<64>, String<8>)> {
    let (domain, tld) = (names.next()?.ok()?, names.next()?.ok()?);
    Some((
        String::try_from(core::str::from_utf8(domain).ok()?).ok()?,
        String::try_from(core::str::from_utf8(tld).ok()?).ok()?,
    ))
}

#[embassy_executor::task]
/// Responds to MDNS queries, so that the device is reachable via the ".local" address
pub(crate) async fn mdns_responder_task(
    stack: &'static Stack<WifiDevice<'static, WifiStaDevice>>,
    hostname: &'static str,
) {
    info!("Starting MDNS responder task - announcing {hostname}.local");
    // The domain and tld in DNS requests are separated by unusual ASCII symbols
    let mut dns_url_string = String::<64>::new();
    write!(dns_url_string, "\x10{}\x05local\0", hostname).expect("hostname is too long");
    let dns_url_len = dns_url_string.len();

    let mut rx_buffer = [0; 512];
    let mut tx_buffer = [0; 256];
    let mut rx_meta = [PacketMetadata::EMPTY; 16];
    let mut tx_meta = [PacketMetadata::EMPTY; 16];

    loop {
        if stack.is_link_up() {
            break;
        }
        Timer::after(Duration::from_millis(500)).await;
    }
    stack
        .join_multicast_group(IpAddress::Ipv4(Ipv4Address([224, 0, 0, 251])))
        .await
        .unwrap();
    stack.wait_config_up().await;
    let addr_bytes = stack.config_v4().unwrap().address.address().0;

    let mut socket = UdpSocket::new(
        stack,
        &mut rx_meta,
        &mut rx_buffer,
        &mut tx_meta,
        &mut tx_buffer,
    );
    socket.bind(5353).expect("Socket 5353 already in use");
    debug!("UDP socket created");

    loop {
        let mut buf = [0; 512];
        let Ok((_n, _ep)) = socket.recv_from(&mut buf).await else {
            error!("can't read UDP multicast packet");
            continue;
        };
        if let Ok(mut dns_packet) = smoltcp::wire::DnsPacket::new_checked(&mut buf) {
            let Some((domain, tld)) =
                dns_names_to_string(dns_packet.parse_name(dns_packet.payload()))
            else {
                // If we can't parse this MDNS packet, it is also not relevant for us
                continue;
            };
            trace!("mdns packet for {domain}.{tld}");

            if domain == hostname && tld == "local" {
                info!("MDNS query for: {domain}.{tld}");
                dns_packet.set_flags(DnsFlags::RESPONSE | DnsFlags::AUTHORITATIVE);
                dns_packet.set_answer_record_count(1);
                dns_packet.set_question_count(0);
                dns_packet.set_authority_record_count(0);
                dns_packet.set_additional_record_count(0);
                let payload = dns_packet.payload_mut();
                // Trick: we don't need to set the hostname in the packet, as it is already set in the request.

                if payload[dns_url_len] >> 7 == 1 {
                    error!("multicast mdns response expected: Ignoring request");
                    continue;
                }

                // Record type A
                payload[dns_url_len] = 0;
                payload[dns_url_len + 1] = 1;
                // Class & cache flush
                payload[dns_url_len + 2] = 0x80;
                payload[dns_url_len + 3] = 0x01;
                // TTL: ~120s~ 30s
                payload[dns_url_len + 4] = 0x00;
                payload[dns_url_len + 5] = 0x00;
                payload[dns_url_len + 6] = 0x00;
                payload[dns_url_len + 7] = 0x28;
                // Data length (4)
                payload[dns_url_len + 8] = 0x00;
                payload[dns_url_len + 9] = 0x04;
                // IP
                payload[dns_url_len + 10] = addr_bytes[0];
                payload[dns_url_len + 11] = addr_bytes[1];
                payload[dns_url_len + 12] = addr_bytes[2];
                payload[dns_url_len + 13] = addr_bytes[3];

                Timer::after(Duration::from_millis(20)).await;
                if let Err(e) = socket
                    // lenght of header (12 bytes) + DNS A record response lenght
                    .send_to(
                        &buf[..12 + dns_url_len + 14],
                        IpEndpoint::new(IpAddress::Ipv4(Ipv4Address([224, 0, 0, 251])), 5353),
                    )
                    .await
                {
                    error!("Can't send to socket: {e:?}");
                    continue;
                }
            }
        }
    }
}
