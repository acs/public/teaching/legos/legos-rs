pub const INORM_IN_S126ATYG: f32 = 100.0 / 35.0 * 20.0; //all values in mA
pub const INORM_IN_S126ATY: f32 = 100.0 / 120.0 * 20.0;
pub const INORM_IN_S126ATA: f32 = 100.0 / 140.0 * 20.0;
pub const INORM_IN_S126ATR: f32 = 100.0 / 140.0 * 20.0;
pub const INORM_IN_S126ATB: f32 = 100.0 / 140.0 * 20.0;
pub const INORM_IN_S126ATG: f32 = 100.0 / 560.0 * 20.0;
pub const INORM_IN_S126ATUW: f32 = 100.0 / 180.0 * 20.0;
