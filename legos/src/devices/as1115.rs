//! Driver for the AS1115 7-Segment LED driver with three displays attached.

use embedded_hal_async::i2c::I2c;
use heapless::Vec;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

#[derive(Debug, Copy, Clone)]
pub enum AS1115Error {
    I2CError,
    InvalidInput,
    InvalidDisplay,
}

#[allow(non_camel_case_types, clippy::upper_case_acronyms)]
#[allow(dead_code)]
#[repr(u8)]
enum Registers {
    DIGIT0 = 0x1,
    DIGIT1 = 0x2,
    DIGIT2 = 0x3,
    DIGIT3 = 0x4,
    DIGIT4 = 0x5,
    DIGIT5 = 0x6,
    DIGIT6 = 0x7,
    DIGIT7 = 0x8,
    DECODE_MODE = 0x9,
    GLOBAL_INTENSITY = 0xA,
    SCAN_LIMIT = 0xB,
    SHUTDOWN = 0xC,
    FEATURE = 0xE,
    DISPLAY_TEST_MODE = 0xF,
    DIG0_1_INTENSITY = 0x10,
    DIG2_3_INTENSITY = 0x11,
    DIG4_5_INTENSITY = 0x12,
    DIG6_7_INTENSITY = 0x13,
    DIAGNOSTIC_DIGIT0 = 0x14,
    DIAGNOSTIC_DIGIT1 = 0x15,
    DIAGNOSTIC_DIGIT2 = 0x16,
    DIAGNOSTIC_DIGIT3 = 0x17,
    DIAGNOSTIC_DIGIT4 = 0x18,
    DIAGNOSTIC_DIGIT5 = 0x19,
    DIAGNOSTIC_DIGIT6 = 0x1A,
    DIAGNOSTIC_DIGIT7 = 0x1B,
    KEYA = 0x1C,
    KEYB = 0x1D,
    SELF_ADDRESSING = 0x2D,
}

/// Three 7-Segment displays connected to a AS1115 driver
pub struct AS1115<I2C: I2c> {
    i2c: I2C,
    addr: u8,
}
impl<I2C: I2c> AS1115<I2C> {
    pub async fn new(i2c: I2C, addr: u8) -> Result<Self, AS1115Error> {
        let mut as1115 = Self { i2c, addr };
        as1115.init().await?;
        Ok(as1115)
    }

    /// Displays `digit` on display number `display_nr`.
    pub async fn display_digit(
        &mut self,
        digit: u8,
        display_nr: u8,
        decimal_point: bool,
    ) -> Result<(), AS1115Error> {
        if digit > 9 {
            return Err(AS1115Error::InvalidInput);
        };
        let mut reg_data = digit;
        if decimal_point {
            reg_data |= 0b10000000;
        }
        //debug!("Displaying {reg_data:b} on display {display_nr}");
        match display_nr {
            0 => {
                self.i2c_write(Registers::DIGIT0, &[reg_data]).await?;
            }
            1 => {
                self.i2c_write(Registers::DIGIT1, &[reg_data]).await?;
            }
            2 => {
                self.i2c_write(Registers::DIGIT2, &[reg_data]).await?;
            }
            _ => return Err(AS1115Error::InvalidDisplay),
        }
        Ok(())
    }

    /// Displays a number between 0.0 and 99.9 on the three displays with one decimal.
    pub async fn display_decimal_number(&mut self, nr: f32) -> Result<(), AS1115Error> {
        //debug!("7-Segment Displaying {nr}");
        if !(0.0..=100.0).contains(&nr) {
            return Err(AS1115Error::InvalidInput);
        }
        let display_nr = (nr * 10.0) as u16;
        //debug!("display_nr: {display_nr}");
        self.display_digit(((display_nr / 100) % 10) as u8, 0, false)
            .await?;
        self.display_digit(((display_nr / 10) % 10) as u8, 1, true)
            .await?;
        self.display_digit((display_nr % 10) as u8, 2, false)
            .await?;

        Ok(())
    }

    // set the display brightness between 0 and 16.
    pub async fn set_brightness(&mut self, brightness: u8) -> Result<(), AS1115Error> {
        if brightness > 16 {
            return Err(AS1115Error::InvalidInput);
        };
        self.i2c_write(Registers::GLOBAL_INTENSITY, &[brightness])
            .await?;
        Ok(())
    }

    async fn init(&mut self) -> Result<(), AS1115Error> {
        debug!("Initializing AS1115 at address {:x}", self.addr);
        // wake up
        self.i2c_write(Registers::SHUTDOWN, &[0b00000001]).await?;
        // use 7-Segment encoding
        self.i2c_write(Registers::DECODE_MODE, &[0b00000111])
            .await?;
        // Only use first three registers
        self.i2c_write(Registers::SCAN_LIMIT, &[0b00000011]).await?;
        self.set_brightness(15).await?;
        self.display_decimal_number(0.0).await?;

        Ok(())
    }

    async fn i2c_write(&mut self, addr: Registers, data: &[u8]) -> Result<(), AS1115Error> {
        let mut cmd = Vec::<u8, 8>::new();
        cmd.push(addr as u8).unwrap();
        cmd.extend_from_slice(data).unwrap();
        self.i2c
            .write(self.addr, &cmd)
            .await
            .map_err(|_e| AS1115Error::I2CError)?;
        Ok(())
    }
}
