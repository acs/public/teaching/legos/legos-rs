use embedded_hal_async::i2c::I2c;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

#[derive(Debug, Copy, Clone)]
pub enum TMP100Error {
    I2CError,
}

#[allow(non_camel_case_types, clippy::upper_case_acronyms)]
#[allow(dead_code)]
#[repr(u8)]
enum Registers {
    TEMP = 0x00,
    CONFIG = 0x01,
    T_LOW = 0x02,
    T_HIGH = 0x03,
}

pub struct TMP100<I2C: I2c> {
    i2c: I2C,
    addr: u8,
}
impl<I2C: I2c> TMP100<I2C> {
    pub async fn new(i2c: I2C, addr: u8) -> Result<Self, TMP100Error> {
        let tmp = Self { i2c, addr };
        Ok(tmp)
    }

    /// Reads temperature from the temperature register. According to default configuration, this measruement is taken in °C
    pub async fn read_temperature(&mut self) -> Result<u8, TMP100Error> {
        let mut target = [0];

        self.i2c_read(Registers::TEMP, &mut target).await?;
        //debug!("TEMPERATUR: {target:?}");

        Ok(target[0])
    }

    /// Note that for block reads, `target` must be one byte larger than the expected data, as the first byte is the length of the data
    async fn i2c_read(&mut self, addr: Registers, target: &mut [u8]) -> Result<(), TMP100Error> {
        self.i2c
            .write(self.addr, &[addr as u8])
            .await
            .map_err(|_e| TMP100Error::I2CError)?;
        self.i2c
            .read(self.addr, target)
            .await
            .map_err(|_e| TMP100Error::I2CError)?;
        Ok(())
    }
}
