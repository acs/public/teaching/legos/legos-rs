//! A configurable load, that can sink a configurable amount of power.

use embedded_hal::digital::OutputPin;
use esp_hal::{
    analog::dac::{Dac, Instance},
    gpio::AnalogPin,
};
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

#[derive(Debug, Copy, Clone)]
pub enum VirtualLoadError {
    InvalidInput,
}

/// A configurable load, that can sink a configurable amount of power.
pub struct VirtualLoad<'a, APin: Instance, OePin: OutputPin>
where
    <APin as Instance>::Pin: AnalogPin,
{
    load_r: f32,
    load_i_max: f32,
    dac: Dac<'a, APin>,
    load_enable: Option<OePin>,
}
impl<'a, APin: Instance, OePin: OutputPin> VirtualLoad<'a, APin, OePin>
where
    <APin as Instance>::Pin: AnalogPin,
{
    /// Creates a new virtual_load that can sink up to `load_i_max` Ampere.
    /// `load_r` is the connected resistor value in Ohm. (As this is dual
    /// channel, there are two of these resistors on the pcb)
    pub fn new(
        mut dac: Dac<'a, APin>,
        load_r: f32,
        load_i_max: f32,
        mut load_enable: Option<OePin>,
    ) -> Self {
        dac.write(0);
        if let Some(enable_pin) = load_enable.as_mut() {
            enable_pin.set_low().unwrap();
        }

        VirtualLoad {
            load_r,
            load_i_max,
            dac,
            load_enable,
        }
    }

    /// Sets the target current through the load to the set amount.
    ///
    /// `target_i` is the desired current in mA.
    /// 100 mA in this function produce 94 mA current in the test house
    pub async fn set_v_load_i(&mut self, target_i: f32) {
        if target_i > self.load_i_max * 1000.0 {
            error!("Tried setting load to {target_i} mA. This incident will be reported.");
            return;
        }
        let target_u = if target_i > 0.0 {
            if let Some(enable_pin) = self.load_enable.as_mut() {
                enable_pin.set_high().unwrap();
            }
            self.load_r * target_i / 1000.0
        } else {
            if let Some(enable_pin) = self.load_enable.as_mut() {
                enable_pin.set_low().unwrap();
            }
            0.0
        };
        //debug!("setting virtual load to {target_i:.0} mA ({target_u:.2} V)");
        if (0.0..3.3).contains(&target_u) {
            self.dac.write((target_u / 3.3 * 256.0) as u8)
        } else {
            error!("Tried setting DAC2 to {target_u} V This incident will be reported.");
        }
    }
}
