use embedded_hal_async::spi::SpiDevice;

use crate::devices::ad8400::AD8400;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

pub struct PowerOutput<SPIDEV1: SpiDevice, SPIDEV2: SpiDevice> {
    ad8400_v: AD8400<SPIDEV1>,
    ad8400_i: AD8400<SPIDEV2>,
    r_ref_i: f32, // The resistor value that is added to the ad8400_i resistance
    r_ref_v: f32, // The resistor value that is added to the ad8400_v resistance
}
impl<SPIDEV1: SpiDevice, SPIDEV2: SpiDevice> PowerOutput<SPIDEV1, SPIDEV2> {
    /// Create a new power output instance.
    ///
    /// resisor values (`r_ref_i/v`) are in Ohms.
    pub fn new(spi_dev_v: SPIDEV1, spi_dev_i: SPIDEV2, r_ref_i: f32, r_ref_v: f32) -> Self {
        let ad8400_v = AD8400::new(spi_dev_v);
        let ad8400_i = AD8400::new(spi_dev_i);
        PowerOutput {
            ad8400_v,
            ad8400_i,
            r_ref_i,
            r_ref_v,
        }
    }

    /// Set the voltage of the power output to `voltage` Volts.
    pub async fn set_voltage(&mut self, voltage: f32) {
        let v_min = (self.r_ref_v + 50.0) * 0.000050;
        let v_max = (self.r_ref_v + 50.0 + 10000.0) * 0.000050;
        if voltage < v_min || voltage > v_max {
            error!("Tried setting power_output to {voltage:.3} V. Bounds are {v_min:.1} V to {v_max:.1}V.");
            return;
        }

        // v_set = 50uA * (60k + r_set - 50) (the AD8400 has a base resistance of 50 Ohm)
        let r_set = (voltage / (0.000050)) - self.r_ref_v - 50.0;

        let r_rel = (r_set / 10_000.0 * 255.0) as u8;

        // debug!("Setting voltage: Voltage {voltage:.3} V, r_set: {r_set:.0}, relative resistance: {r_rel}, r_ref: {:.1}", self.r_ref_v);
        self.ad8400_v.set_r_a_w(r_rel).await;
    }

    /// current in mA
    pub async fn set_max_current(&mut self, current: f32) {
        let min_current = (self.r_ref_i - 450.0) / 1000.0 * 360.0;
        assert!(current >= min_current, "Can't limit current to {current:.1}mAh, with r_ref of {:.0} Ohm, we can only limit the current to {min_current:.1}mAh", self.r_ref_i);
        let r_ilim = (current / 360.0) * 1000.0 + (450.0 - self.r_ref_i);
        if r_ilim > 1000.0 {
            error!("Tried setting power_output to {current:.1} mA. The 1kOhm potentiometer cant supply this setting");
            return;
        }
        let val = if current > 0.0 {
            // TODO: A better calibration is needed
            // note: The AD8400 has a base resistance of 50 Ohm
            // the other 40 ohm can't be explained...
            ((r_ilim - 90.0) / 1000.0 * 255.0) as u8
        } else {
            // Ensure that 0 mA is really 0 mA
            0
        };
        self.ad8400_i.set_r_b_w(val).await;
    }
}
