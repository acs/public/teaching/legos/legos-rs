use crate::power::PowerReading;
use embedded_hal_async::i2c::I2c;
use heapless::Vec;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

#[derive(Debug, Copy, Clone)]
pub enum INA233Error {
    I2CError,
    InvalidCalibration,
    InvalidDevice,
    InvalidAlert,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum AlertType {
    Overcurrent,
    Overvoltage,
}

#[allow(non_camel_case_types, clippy::upper_case_acronyms)]
#[allow(dead_code)]
#[repr(u8)]
enum Registers {
    /// Clears the status registers and rearms the black box registers for udpating - SB 0 N/A
    CLEAR_FAULTS = 0x03,
    /// Restores internal registers to the default values SB - 0 N/A
    RESTORE_DEFAULT_ALL = 0x12,
    /// Retrieves the device capability - R 1 0xB0
    CAPABILITY = 0x19,
    /// Retrieves or stores the output overcurrent warn limit threshold - R/W 2 0x7FF8
    IOUT_OC_WARN_LIMIT = 0x4A,
    /// Retrieves or stores the input overvoltage warn limit threshold - R/W 2 0x7FF8
    VIN_OV_WARN_LIMIT = 0x57,
    /// Retrieves or stores the input undervoltage warn limit threshold - R/W 2 0x0000
    VIN_UV_WARN_LIMIT = 0x58,
    /// Retrieves or stores the output overpower warn limit threshold - R/W 2 0x7FF8
    PIN_OP_WARN_LIMIT = 0x6B,
    /// Retrieves information about the device operating status - R 1 0x00
    STATUS_BYTE = 0x78,
    /// Retrieves information about the device operating status - R 2 0x1000
    STATUS_WORD = 0x79,
    /// Retrieves information about the output current status - R/W,CLR 1 0x00
    STATUS_IOUT = 0x7B,
    /// Retrieves information about the input status - R/W,CLR 1 0x00
    STATUS_INPUT = 0x7C,
    /// Retrieves information about the communications status - R/W,CLR 1 0x00
    STATUS_CML = 0x7E,
    /// Retrieves information about the manufacturer specific device status - R/W,CLR 1 0x20
    STATUS_MFR_SPECIFIC = 0x80,
    /// Retrieves the energy reading measurement - Block_R 6 0x00
    READ_EIN = 0x86,
    /// Retrieves the measurement for the VBUS voltage - R 2 0x0000
    READ_VIN = 0x88,
    /// Retrieves the input current measurement, supports both + and - currents - R 2 0x0000
    READ_IIN = 0x89,
    /// Mirrors READ_VIN - R 2 0x0000
    READ_VOUT = 0x8B,
    /// Mirrors READ_IIN for compatibility - R 2 0x0000
    READ_IOUT = 0x8C,
    /// Mirrors READ_PIN for compatibility with possible VBUS connections - R 2 0x0000
    READ_POUT = 0x96,
    /// Retrieves the input power measurement - R 2 0x0000
    READ_PIN = 0x97,
    /// Retrieves the manufacturer ID in ASCII Characters (TI) - Block_R 2 0x54, 0x49
    MFR_ID = 0x99,
    /// Retrieves the device number in ASCII Characters (INA233) - Block_R 6 0x49, 0x4E, 0x41, 0x32, 0x33, 0x33
    MFR_MODEL = 0x9A,
    /// Retrieves the device revision letter and number in ASCII (e.g.A0) - R 2 0x41,0x30
    MFR_REVISION = 0x9B,
    /// Configures the ADC averaging modes, conversion times, and opr. modes - R/W 2 0x4127
    MFR_ADC_CONFIG = 0xD0,
    /// Retrieves the shunt voltage measurement - R 2 0x0000
    MFR_READ_VSHUNT = 0xD1,
    /// Allows masking of device warnings - R/W 1 0xF0
    MFR_ALERT_MASK = 0xD2,
    /// Allows the value of the current-sense resistor calibration value to be input. Must be programed at power-up. Default value is set to 1. - R/W 2 0x0001
    MFR_CALIBRATION = 0xD4,
    /// Allows the ALERT pin polarity to be changed - R/W 1 0x02
    MFR_DEVICE_CONFIG = 0xD5,
    /// Clears the energy accumulator - S_B 0 N/A
    CLEAR_EIN = 0xD6,
    /// Returns a unique word for the manufacturer ID - R 2 0x5449 (TI in ASCII)
    TI_MFR_ID = 0xE0,
    /// Returns a unique word for the manufacturer model - R 2 33 in ASCII
    TI_MFR_MODEL = 0xE1,
    /// Returns a unique word for the manufacturer revision - R 2 A0 in ASCII
    TI_MFR_REVISION = 0xE2,
}

// The INA233 follows the PMBus scheme of interaction, that is:
// Reading:
// - first do a I2C write with the register address
// - second: do a new I2C read to obtain the value(s)
//   - Block reads are preceeded with the Nr of bytes that are transmitted
// Writing:
// - Write register address followed by the data in a single transfer

pub struct INA233<I2C: I2c> {
    i2c: I2C,
    addr: u8,
    /// How many bits per unit (inverse of 'm' in the datasheet)
    conv_i: f32,
    conv_p: f32,
    /// Offset Factors
    offset_factor_v: f32,
    offset_factor_i: f32,
}
impl<I2C: I2c> INA233<I2C> {
    /// `r_shunt` in Ohm, `i_max` in Ampere.
    pub async fn new(i2c: I2C, addr: u8, r_shunt: f32, i_max: f32) -> Result<Self, INA233Error> {
        let mut ina = Self {
            i2c,
            addr,
            conv_i: 0.0,
            conv_p: 0.0,
            offset_factor_v: 1.0,
            offset_factor_i: 1.0,
        };
        let mut dev_name = [0; 7];
        ina.pmbus_read(Registers::MFR_MODEL, &mut dev_name).await?;
        if dev_name != [6, b'I', b'N', b'A', b'2', b'3', b'3'] {
            error!("Not a valid INA233 at addr {addr:x} (MFR_MODEL register is {dev_name:x?})");
            return Err(INA233Error::InvalidDevice);
        }
        ina.set_shunt_imax(r_shunt, i_max).await?;
        Ok(ina)
    }

    /// `r_shunt` in Ohm, `i_max` in Ampere.
    pub async fn set_shunt_imax(&mut self, r_shunt: f32, i_max: f32) -> Result<(), INA233Error> {
        let c_lsb = i_max / 2_u16.pow(15) as f32;
        if c_lsb >= u16::MAX as f32 {
            return Err(INA233Error::InvalidCalibration);
        }
        let cal = (0.00512 / (c_lsb * r_shunt)) as u16;

        debug!("cal: {cal} (as bytes: {:x?}", cal.to_be_bytes());
        self.pmbus_write(Registers::MFR_CALIBRATION, &cal.to_be_bytes())
            .await?;

        // clear possible alerts
        self.clear_alert().await.ok();

        self.conv_i = c_lsb;
        self.conv_p = c_lsb * 25.0;
        Ok(())
    }

    /// Sets offset factors for voltage and current
    pub async fn set_calibration(
        &mut self,
        offset_factor_v: f32,
        offset_factor_i: f32,
    ) -> Result<(), INA233Error> {
        debug!("Calibrating offsets for INA233 ({:#x}): voltage offset: {offset_factor_v}, current offset: {offset_factor_i}", self.addr);
        self.offset_factor_v = offset_factor_v;
        self.offset_factor_i = offset_factor_i;
        Ok(())
    }

    pub async fn set_alert(&mut self, i_max: f32, v_max: f32) -> Result<(), INA233Error> {
        debug!(
            "Programming alerts for INA233 ({:#x}): I_max: {:0.4}, V_max: {:0.4}",
            self.addr, i_max, v_max
        );
        if self.conv_i == 0.0 {
            return Err(INA233Error::InvalidCalibration);
        }

        // Apply Offset Factors
        let v_max: f32 = v_max / self.offset_factor_v;
        let i_max: f32 = i_max / self.offset_factor_i;

        let cal_v = (800.0 * v_max) as u16;
        let cal_i = (i_max * 8.0 / self.conv_i) as u16;
        // I have no fucking clue, why we need le bytes here, but be bytes for the calibration
        self.pmbus_write(Registers::VIN_OV_WARN_LIMIT, &cal_v.to_le_bytes())
            .await?;
        self.pmbus_write(Registers::IOUT_OC_WARN_LIMIT, &cal_i.to_le_bytes())
            .await?;

        // Unmask the alerts IN_OC_WARNING & VIN_OV_WARN
        self.pmbus_write(Registers::MFR_ALERT_MASK, &[0b11111001])
            .await?;

        Ok(())
    }

    pub async fn check_alert(&mut self) -> Result<AlertType, INA233Error> {
        trace!("checking alert");
        let mut status_reg = [0; 1];
        self.pmbus_read(Registers::STATUS_MFR_SPECIFIC, &mut status_reg)
            .await?;
        let alert_type = if status_reg[0] >> 1 & 0b1 == 0b1 {
            AlertType::Overvoltage
        } else if status_reg[0] >> 2 & 0b1 == 0b1 {
            AlertType::Overcurrent
        } else {
            return Err(INA233Error::InvalidAlert);
        };
        Ok(alert_type)
    }

    pub async fn clear_alert(&mut self) -> Result<(), INA233Error> {
        trace!("clearing alert");
        self.pmbus_write(Registers::CLEAR_FAULTS, &[]).await?;
        Ok(())
    }

    pub async fn read_power(&mut self) -> Result<PowerReading, INA233Error> {
        let mut buf = [0; 2];
        self.pmbus_read(Registers::READ_IIN, &mut buf).await?;
        let mut current = i16::from_le_bytes(buf) as f32 * self.conv_i / 8.0;
        self.pmbus_read(Registers::READ_VIN, &mut buf).await?;
        let mut voltage = i16::from_le_bytes(buf) as f32 / 800.0;
        self.pmbus_read(Registers::READ_PIN, &mut buf).await?;
        let mut power = i16::from_le_bytes(buf) as f32 * self.conv_p / 8.0;

        // Apply Offset Factors
        voltage *= self.offset_factor_v;
        current *= self.offset_factor_i;
        power = power * self.offset_factor_v * self.offset_factor_i;

        Ok(PowerReading {
            voltage,
            current,
            power,
        })
    }

    /// Note that for block reads, `target` must be one byte larger than the expected data, as the first byte is the length of the data
    async fn pmbus_read(&mut self, addr: Registers, target: &mut [u8]) -> Result<(), INA233Error> {
        self.i2c
            .write(self.addr, &[addr as u8])
            .await
            .map_err(|_e| INA233Error::I2CError)?;
        self.i2c
            .read(self.addr, target)
            .await
            .map_err(|_e| INA233Error::I2CError)?;
        Ok(())
    }

    async fn pmbus_write(&mut self, addr: Registers, data: &[u8]) -> Result<(), INA233Error> {
        let mut cmd = Vec::<u8, 8>::new();
        cmd.push(addr as u8).unwrap();
        cmd.extend_from_slice(data).unwrap();
        self.i2c
            .write(self.addr, &cmd)
            .await
            .map_err(|_e| INA233Error::I2CError)?;
        Ok(())
    }
}
