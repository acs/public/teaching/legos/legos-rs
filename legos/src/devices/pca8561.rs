use embedded_hal_async::i2c::I2c;
use esp_hal::gpio::{GpioPin, Output};
use heapless::Vec;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

#[derive(Debug, Copy, Clone)]
pub enum PCA8561Error {
    I2CError,
    InvalidInput,
}

#[allow(non_camel_case_types, clippy::upper_case_acronyms)]
#[allow(dead_code)]
#[repr(u8)]
enum Registers {
    RESET = 0x00,
    DEVICE_CTRL = 0x01,
    DISPLAY_CTRL_1 = 0x02,
    DISPLAY_CTRL_2 = 0x03,
    COM0_0 = 0x04,
    COM0_1 = 0x05,
    COM0_2 = 0x06,
    COM1_0 = 0x07,
    COM1_1 = 0x08,
    COM1_2 = 0x09,
    COM2_0 = 0x0A,
    COM2_1 = 0x0B,
    COM2_2 = 0x0C,
    COM3_0 = 0x0D,
    COM3_1 = 0x0E,
    COM3_2 = 0x0F,
}

const BITS_0: [u8; 3] = [0b1110, 0b01100000, 0b1];
const BITS_1: [u8; 3] = [0b0010, 0b00100000, 0b0];
const BITS_2: [u8; 3] = [0b1100, 0b10100000, 0b1];
const BITS_3: [u8; 3] = [0b0110, 0b10100000, 0b1];
const BITS_4: [u8; 3] = [0b0010, 0b11100000, 0b0];
const BITS_5: [u8; 3] = [0b0110, 0b11000000, 0b1];
const BITS_6: [u8; 3] = [0b1110, 0b11000000, 0b1];
const BITS_7: [u8; 3] = [0b0010, 0b00100000, 0b1];
const BITS_8: [u8; 3] = [0b1110, 0b11100000, 0b1];
const BITS_9: [u8; 3] = [0b0110, 0b11100000, 0b1];

pub struct PCA8561<'a, I2C: I2c> {
    i2c: I2C,
    addr: u8,
    pin_rst: Output<'a, GpioPin<32>>,
}
impl<'a, I2C: I2c> PCA8561<'a, I2C> {
    pub async fn new(
        i2c: I2C,
        addr: u8,
        pin_rst: Output<'a, GpioPin<32>>,
    ) -> Result<Self, PCA8561Error> {
        let mut pca = Self { i2c, addr, pin_rst };
        pca.pin_rst.set_high();
        pca.init().await?;
        Ok(pca)
    }

    pub async fn display_number(
        &mut self,
        nr: u8,
        display_port: u8,
        enable_decimal_point: bool,
    ) -> Result<(), PCA8561Error> {
        let com_data = match nr {
            0 => BITS_0,
            1 => BITS_1,
            2 => BITS_2,
            3 => BITS_3,
            4 => BITS_4,
            5 => BITS_5,
            6 => BITS_6,
            7 => BITS_7,
            8 => BITS_8,
            9 => BITS_9,
            _ => return Err(PCA8561Error::InvalidInput),
        };
        let com_data_0 = com_data[0] + if enable_decimal_point { 0b00000001 } else { 0 };
        match display_port {
            0 => {
                self.pmbus_write(Registers::COM0_0, &[com_data_0]).await?;
                self.pmbus_write(Registers::COM0_1, &[com_data[1]]).await?;
                self.pmbus_write(Registers::COM0_2, &[com_data[2]]).await?;
            }
            1 => {
                self.pmbus_write(Registers::COM1_0, &[com_data_0]).await?;
                self.pmbus_write(Registers::COM1_1, &[com_data[1]]).await?;
                self.pmbus_write(Registers::COM1_2, &[com_data[2]]).await?;
            }
            2 => {
                self.pmbus_write(Registers::COM2_0, &[com_data_0]).await?;
                self.pmbus_write(Registers::COM2_1, &[com_data[1]]).await?;
                self.pmbus_write(Registers::COM2_2, &[com_data[2]]).await?;
            }
            _ => panic!(),
        }
        Ok(())
    }

    pub async fn display_decimal_number(&mut self, nr: f32) -> Result<(), PCA8561Error> {
        if !(0.0..=100.0).contains(&nr) {
            return Err(PCA8561Error::InvalidInput);
        }
        let display_nr = (nr * 10.0) as u16;
        self.display_number(((display_nr / 100) % 10) as u8, 0, false)
            .await?;
        self.display_number(((display_nr / 10) % 10) as u8, 1, true)
            .await?;
        self.display_number((display_nr % 10) as u8, 2, false)
            .await?;
        Ok(())
    }

    async fn init(&mut self) -> Result<(), PCA8561Error> {
        let mut target = [0];
        self.pmbus_write(Registers::DEVICE_CTRL, &[0x04]).await?;
        self.pmbus_read(Registers::DEVICE_CTRL, &mut target).await?;
        debug!("PCA8561 Device_Ctrl: {target:?}");
        self.pmbus_write(Registers::DISPLAY_CTRL_1, &[0b00000101])
            .await?;
        self.pmbus_read(Registers::DISPLAY_CTRL_1, &mut target)
            .await?;
        debug!("PCA8561 Deplay_Ctrl_1: {target:?}");
        Ok(())
    }

    /// Note that for block reads, `target` must be one byte larger than the expected data, as the first byte is the length of the data
    async fn pmbus_read(&mut self, addr: Registers, target: &mut [u8]) -> Result<(), PCA8561Error> {
        self.i2c
            .write(self.addr, &[addr as u8])
            .await
            .map_err(|_e| PCA8561Error::I2CError)?;
        self.i2c
            .read(self.addr, target)
            .await
            .map_err(|_e| PCA8561Error::I2CError)?;
        Ok(())
    }

    async fn pmbus_write(&mut self, addr: Registers, data: &[u8]) -> Result<(), PCA8561Error> {
        let mut cmd = Vec::<u8, 8>::new();
        cmd.push(addr as u8).unwrap();
        cmd.extend_from_slice(data).unwrap();
        self.i2c
            .write(self.addr, &cmd)
            .await
            .map_err(|_e| PCA8561Error::I2CError)?;
        Ok(())
    }
}
