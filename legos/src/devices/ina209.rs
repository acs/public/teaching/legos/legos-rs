use crate::power::PowerReading;
use embedded_hal_async::i2c::I2c;
use heapless::Vec;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use num_traits::float::FloatCore;

#[derive(Debug, Copy, Clone)]
pub enum INA209Error {
    I2CError,
    InvalidCalibration,
    InvalidDevice,
}

const VSHUNT_MAX: f32 = 0.32;

#[allow(non_camel_case_types, clippy::upper_case_acronyms)]
#[allow(dead_code)]
#[repr(u8)]
enum Registers {
    /// All-register reset, settings for bus voltage range, PGA Gain, ADC resolution/averaging.
    CONFIG = 0x00,
    /// Status flags for warnings, over-/under-limits, conversion ready, math overflow, and SMBus Alert.
    STATUS = 0x01,
    /// Enables/disables flags in the Status Register
    FLAGS = 0x02,
    /// Shunt voltage measurement data
    VSHUNT = 0x03,
    /// Bus voltage measurement data
    VBUS = 0x04,
    /// Power measurement data
    POWER = 0x05,
    /// Contains the value of the current flowing through the shunt resistor.
    CURRENT = 0x06,
    /// Contains most positive voltage reading of Shunt Voltage Register.
    VSHUNT_PEAK_POS = 0x07,
    /// Contains most negative voltage reading of Shunt Voltage Register.
    VSHUNT_PEAK_NEG = 0x08,
    /// Contains highest voltage reading of Bus Voltage Register.
    VBUS_PEAK_POS = 0x09,
    /// Contains lowest voltage reading of Bus Voltage Register.
    VBUS_PEAK_NEG = 0x0A,
    /// Contains highest power reading of Power Register.
    POWER_PEAK = 0x0B,
    /// Warning watchdog, sets positive shunt voltage limit that triggers warning flag in status register and activates warning pin
    VSHUNT_WARNING_POS = 0x0C,
    /// Warning watchdog, sets negative shunt voltage limit that triggers warning flag in status register and activates warning pin
    VSHUNT_WARNING_NEG = 0x0D,
    /// Warning watchdog, sets power limit that triggers warning flag in status register and activates warning pin
    POWER_WARNING = 0x0E,
    /// Warning watchdog, sets high bus voltage limit that triggers warning flag in status register and activates warning pin
    BUS_OVERVOLTAGE_WARNING = 0x0F,
    /// Warning watchdog, sets low bus voltage limit that triggers warning flag in status register and activates warning pin
    BUS_UNDERVOLTAGE_WARNING = 0x10,
    /// Overlimit watchdog, sets power limit that triggers overlimit flag in status register and activates overlimit pin
    POWER_OVERLIMIT = 0x11,
    /// Overlimit watchdog, sets bus overvoltage limit that triggers overlimit flag in status register and activates overlimit pin
    BUS_OVERLIMIT = 0x12,
    /// Overlimit watchdog, sets bus undervoltage limit that triggers overlimit flag in status register and activates overlimit pin
    BUS_UNDERLIMIT = 0x13,
    /// Sets positiv limit for internal critical DAC+
    CRITICAL_DAC_POS = 0x14,
    /// Sets negativ limit for internal critical DAC+
    CRITICAL_DAC_NEG = 0x15,
    /// Sets full-scale range and LSB of current and power measurements. Overall system calibration.
    CALIBRATION = 0x16,
}

// The INA233 follows the PMBus scheme of interaction, that is:
// Reading:
// - first do a I2C write with the register address
// - second: do a new I2C read to obtain the value(s)
//   - Block reads are preceeded with the Nr of bytes that are transmitted
// Writing:
// - Write register address followed by the data in a single transfer

pub struct INA209<I2C: I2c> {
    i2c: I2C,
    addr: u8,
    /// How many bits per unit (inverse of 'm' in the datasheet)
    conv_i: f32,
    conv_p: f32,
}
impl<I2C: I2c> INA209<I2C> {
    /// `r_shunt` in Ohm, `i_max` in Ampere.
    pub async fn new(i2c: I2C, addr: u8, r_shunt: f32, i_max: f32) -> Result<Self, INA209Error> {
        let mut ina = Self {
            i2c,
            addr,
            conv_i: 0.0,
            conv_p: 0.0,
        };

        ina.pmbus_write(Registers::CONFIG, &0b0011100110011111_u16.to_be_bytes())
            .await
            .unwrap();
        ina.set_calibration(r_shunt, i_max).await?;
        Ok(ina)
    }

    /// `r_shunt` in Ohm, `i_max` in Ampere.
    pub async fn set_calibration(&mut self, r_shunt: f32, i_max: f32) -> Result<(), INA209Error> {
        assert!(r_shunt > 0.0, "r_shunt must be greater than 0");
        assert!(i_max > 0.0, "i_max must be greater than 0");

        let real_i_max = VSHUNT_MAX / r_shunt;
        assert!(real_i_max < i_max, "Calibration I_Max out-of-range, increase R_sh ({r_shunt} Ohm) or decrease I_max ({i_max} A)");

        let c_lsb: f32 = (i_max / 32767.0 * 100000.0).ceil() / 100000.0;
        debug!("c_lsb: {c_lsb}");
        let cal: f32 = 0.04096 / (c_lsb * r_shunt);
        let cal = cal as i32;

        debug!("cal: {cal}");
        assert!(
            cal < 0xFFFF_i32,
            "Calibration out-of-range, increase R_sh or I_max"
        );

        let cal = cal as u16;
        debug!("cal: {cal} (as bytes: {:x?}", cal.to_be_bytes());

        self.pmbus_write(Registers::CALIBRATION, &cal.to_be_bytes())
            .await?;

        self.conv_i = c_lsb;
        self.conv_p = c_lsb * 20.0;

        Ok(())
    }

    pub async fn read_power(&mut self) -> Result<PowerReading, INA209Error> {
        let mut buf = [0; 2];
        self.pmbus_read(Registers::CURRENT, &mut buf).await?;
        let current = i16::from_be_bytes(buf) as f32 * self.conv_i;
        self.pmbus_read(Registers::VBUS, &mut buf).await?;
        // the LSB (bit 3) is 4mv -> 8/0,004 = 2000
        let voltage = i16::from_be_bytes(buf) as f32 / 2000.0;
        self.pmbus_read(Registers::POWER, &mut buf).await?;
        let power = i16::from_be_bytes(buf) as f32 * self.conv_p;

        Ok(PowerReading {
            voltage,
            current,
            power,
        })
    }

    /// Note that for block reads, `target` must be one byte larger than the expected data, as the first byte is the length of the data
    async fn pmbus_read(&mut self, addr: Registers, target: &mut [u8]) -> Result<(), INA209Error> {
        self.i2c
            .write(self.addr, &[addr as u8])
            .await
            .map_err(|_e| INA209Error::I2CError)?;
        self.i2c
            .read(self.addr, target)
            .await
            .map_err(|_e| INA209Error::I2CError)?;
        Ok(())
    }

    async fn pmbus_write(&mut self, addr: Registers, data: &[u8]) -> Result<(), INA209Error> {
        let mut cmd = Vec::<u8, 8>::new();
        cmd.push(addr as u8).unwrap();
        cmd.extend_from_slice(data).unwrap();
        self.i2c
            .write(self.addr, &cmd)
            .await
            .map_err(|_e| INA209Error::I2CError)?;
        Ok(())
    }
}
