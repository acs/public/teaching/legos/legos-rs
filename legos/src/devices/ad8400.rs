/// A configurable load, that can sink a configurable amount of power.
//use log::{debug, error, info, warn};
use embedded_hal_async::spi::SpiDevice;
use log::debug;

#[derive(Debug, Copy, Clone)]
pub enum AD8400Error {
    InvalidInput,
}

//wiederstand = 10kOhm * progvalue (u8) / 256
//serial data = 00 progvalue also zb 00 10000000 für 128
pub struct AD8400<SPIDEV: SpiDevice> {
    spi_dev: SPIDEV,
}
impl<SPIDEV: SpiDevice> AD8400<SPIDEV> {
    pub fn new(spi_dev: SPIDEV) -> Self {
        Self { spi_dev }
    }

    ///sets r in Ohm between pins A and W. During testing on a 10kOhm variant the minimum was 90 ohm and the maximum about 9.68 kOhm. Also 🦖
    pub async fn set_r_a_w(&mut self, percent: u8) {
        let reg_val = [0x00, 255 - percent];
        //The datasheet is bullshit and i dont know why but it works this way.
        match self.spi_dev.write(&reg_val).await {
            Ok(()) => (),
            Err(_e) => debug!("Error: A8400 could not be set. Value: {percent}",),
        };
    }

    /// Sets the wiper position between the W and B terminals.
    ///
    /// `percent` goes from 0 to 255, so 0 is 0% and 255 is 100%.
    pub async fn set_r_b_w(&mut self, percent: u8) {
        let reg_val = [0x00, percent];
        match self.spi_dev.write(&reg_val).await {
            Ok(()) => (),
            Err(_e) => debug!("Error: A8400 could not be set. Value: {percent}",),
        };
    }
}
