use embedded_hal::digital::OutputPin;
use embedded_hal_async::i2c::I2c;
use heapless::Vec;
use log::{debug, error};

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum PCA9956Error {
    I2CError,
    InvalidDevice,
    PCAErrorFlag,
    InvalidArgument,
    OutOfBound,
}

#[allow(non_camel_case_types, clippy::upper_case_acronyms)]
#[allow(dead_code)]
#[repr(u8)]
enum Registers {
    /// Mode register 1
    MODE_1 = 0x00,
    /// Mode register 2
    MODE_2 = 0x01,
    /// LED output state 0
    LEDOUT_0 = 0x02,
    /// Group duty cycle control
    GROUP_PWM = 0x08,
    /// Group frequency
    GROUP_FREQ = 0x09,
    /// Brightness control LED0
    PWM_0 = 0x0A,
    /// Output gain control register 0
    IREF_0 = 0x22,
    /// Offset/Delay on LED outputs
    OFFSET = 0x3A,
    /// I2C-bus subaddress 1
    SUBADR1 = 0x3B,
    /// I2C-bus subaddress 2
    SUBADR2 = 0x3C,
    /// I2C-bus subaddress 3
    SUBADR3 = 0x3D,
    /// All Call I2C-bus address
    ALL_CALLADR = 0x3E,
    /// Brightness control for all LEDs
    ALL_PWM = 0x3F,
    /// Output gain control for all registers IREF0 to IREF23
    ALL_IREF = 0x40,
    /// Output error flag 0
    ERFLAGS_0 = 0x41,
}

pub struct PCA9956<I2C: I2c, PIN: OutputPin> {
    i2c: I2C,
    addr: u8,
    r_ext: f32,
    not_oe_pin: Option<PIN>,
}
#[allow(dead_code)]
impl<I2C: I2c, PIN: OutputPin> PCA9956<I2C, PIN> {
    /// "r_ext" in Ohm, `i_max_ma_array` max current values per LED in milliampere
    pub async fn new(
        i2c: I2C,
        addr: u8,
        r_ext: f32,
        i_max_ma_array: [f32; 24],
        not_oe_pin: Option<PIN>,
    ) -> Result<Self, PCA9956Error> {
        let mut pca = Self {
            i2c,
            addr,
            r_ext,
            not_oe_pin,
        };

        // Check if Chip is present
        let mode_1: u8 = pca.i2c_read(Registers::MODE_1).await?;
        let mode_2: u8 = pca.i2c_read(Registers::MODE_2).await?;
        debug!("PCA9956 Mode 1 register value: {mode_1:#010b}");
        debug!("PCA9956 Mode 2 register value: {mode_2:#010b}");
        if (mode_2 & 0b111) != 0b101 {
            error!("Invalid content of pca9956 mode 2 register!");
            error!("Chip probably broken");
            return Err(PCA9956Error::InvalidDevice);
        }

        // Write irefs in registers and set according LEDOUT-bit to 0 if iref is 0
        for i in 0..6 {
            let mut reg: u8 = 0;
            for j in 0..4 {
                let i_ref = calculate_iref(pca.r_ext, i_max_ma_array[i * 4 + j]);
                pca.i2c_write_raw(Registers::IREF_0 as u8 + (i * 4 + j) as u8, i_ref)
                    .await?;

                if i_ref != 0 {
                    reg |= 0b10 << (j * 2);
                }
            }
            pca.i2c_write_raw(Registers::LEDOUT_0 as u8 + i as u8, reg)
                .await?;
        }

        // output enable:
        if let Some(not_oe_pin) = pca.not_oe_pin.as_mut() {
            not_oe_pin.set_low().unwrap();
        }

        Ok(pca)
    }

    /// Checks for errors and prints error registers if error flag is set (Datasheet section 7.3.14)
    pub async fn error_check(&mut self) -> Result<(), PCA9956Error> {
        let mode_2: u8 = self.i2c_read(Registers::MODE_2).await?;
        debug!("PCA9956 Mode 2 register value: {mode_2:#010b}");
        if (mode_2 & (1 << 6)) != 0 {
            error!("PCA9956 Error flag is set");

            for i in 0..6 {
                let mut eflags = [0];
                self.i2c_read_raw(Registers::ERFLAGS_0 as u8 + i, &mut eflags)
                    .await?;
                error!("EFLAG{i}: {:#010b}", eflags[0]);
            }
            return Err(PCA9956Error::PCAErrorFlag);
        }

        Ok(())
    }

    /// Prints out all registers for debugging
    pub async fn print_registers(&mut self) -> Result<(), PCA9956Error> {
        use Registers::*;
        let reg = self.i2c_read(MODE_1).await?;
        debug!("MODE_1 register: {reg:#010b}");
        let reg = self.i2c_read(MODE_2).await?;
        debug!("MODE_2 register: {reg:#010b}");

        for i in 0..6 {
            let mut regs = [0];
            self.i2c_read_raw(LEDOUT_0 as u8 + i, &mut regs).await?;
            debug!("LEDOUT_{i} register: {:#010b}", regs[0]);
        }

        let reg = self.i2c_read(GROUP_PWM).await?;
        debug!("GROUP_PWM register: {reg:#010b}");
        let reg = self.i2c_read(GROUP_FREQ).await?;
        debug!("GROUP_FREQ register: {reg:#010b}");

        for i in 0..24 {
            let mut regs = [0];
            self.i2c_read_raw(PWM_0 as u8 + i, &mut regs).await?;
            debug!("PWM_{i} register: {:#010b}", regs[0]);
        }

        for i in 0..24 {
            let mut regs = [0];
            self.i2c_read_raw(IREF_0 as u8 + i, &mut regs).await?;
            debug!("IREF_{i} register: {:#010b}", regs[0]);
        }

        let reg = self.i2c_read(OFFSET).await?;
        debug!("OFFSET register: {reg:#010b}");
        let reg = self.i2c_read(SUBADR1).await?;
        debug!("SUBADR1 register: {reg:#010b}");
        let reg = self.i2c_read(SUBADR2).await?;
        debug!("SUBADR2 register: {reg:#010b}");
        let reg = self.i2c_read(SUBADR3).await?;
        debug!("SUBADR3 register: {reg:#010b}");
        let reg = self.i2c_read(ALL_CALLADR).await?;
        debug!("ALL_CALLADR register: {reg:#010b}");
        let reg = self.i2c_read(ALL_PWM).await?;
        debug!("ALL_PWM register: {reg:#010b}");
        let reg = self.i2c_read(ALL_IREF).await?;
        debug!("ALL_IREF register: {reg:#010b}");

        for i in 0..6 {
            let mut regs = [0];
            self.i2c_read_raw(ERFLAGS_0 as u8 + i, &mut regs).await?;
            debug!("ERFLAGS_{i} register: {:#010b}", regs[0]);
        }

        Ok(())
    }

    /// Read byte from register adress addr (Register enum)
    async fn i2c_read(&mut self, addr: Registers) -> Result<u8, PCA9956Error> {
        let mut target = [0];
        self.i2c_read_raw(addr as u8, &mut target).await?;
        Ok(target[0])
    }

    /// Read byte from register adress addr as u8 (not Register enum)
    async fn i2c_read_raw(&mut self, addr: u8, target: &mut [u8]) -> Result<(), PCA9956Error> {
        if addr + target.len() as u8 > 0x46 + 1 {
            return Err(PCA9956Error::OutOfBound);
        }
        self.i2c
            .write(self.addr, &[addr])
            .await
            .map_err(|_e| PCA9956Error::I2CError)?;
        self.i2c
            .read(self.addr, target)
            .await
            .map_err(|_e| PCA9956Error::I2CError)?;
        Ok(())
    }

    /// Writes data (single byte) to register adress addr (Register enum)
    async fn i2c_write(&mut self, addr: Registers, data: u8) -> Result<(), PCA9956Error> {
        self.i2c_write_raw(addr as u8, data).await?;
        Ok(())
    }

    /// Writes data (single byte) to register adress addr as u8 (not Register enum)
    async fn i2c_write_raw(&mut self, addr: u8, data: u8) -> Result<(), PCA9956Error> {
        if addr > 0x46 {
            return Err(PCA9956Error::OutOfBound);
        }
        let cmd = [addr, data];
        self.i2c
            .write(self.addr, &cmd)
            .await
            .map_err(|_e| PCA9956Error::I2CError)?;
        Ok(())
    }

    /// Writes data block (array of bytes) to registers addr and above using the Auto_Increment-Flag (AIF) (Datasheet section 7.2)
    async fn i2c_block_write(&mut self, addr: Registers, data: &[u8]) -> Result<(), PCA9956Error> {
        let mut cmd = Vec::<u8, 32>::new();
        // Bitwise Operation ensures the Auto-Increment-Flag (AIF) is set
        cmd.push(addr as u8 | (1 << 7)).unwrap();
        cmd.extend_from_slice(data).unwrap();
        self.i2c
            .write(self.addr, &cmd)
            .await
            .map_err(|_e| PCA9956Error::I2CError)?;
        Ok(())
    }

    /// Sets the max current for a specific LED channel to i_max_mA (milliampère)
    #[allow(non_snake_case)]
    pub async fn set_max_current(
        &mut self,
        channel: u8,
        i_max_mA: f32,
    ) -> Result<(), PCA9956Error> {
        let iref_register: u8 = Registers::IREF_0 as u8 + channel;
        let iref: u8 = calculate_iref(self.r_ext, i_max_mA);
        debug!("PCA9956 LED {channel} new Current Limit Register: {iref}");
        self.i2c_write_raw(iref_register, iref).await?;
        Ok(())
    }

    /// Set LED specified by channel to value duty
    pub async fn set_led(&mut self, channel: u8, duty: u8) -> Result<(), PCA9956Error> {
        let led_register: u8 = Registers::PWM_0 as u8 + channel;
        self.i2c_write_raw(led_register, duty).await?;
        Ok(())
    }

    /// Set all LED brightnesses to values specified in `duties`.
    /// Fails if `duties` excceeds 24 values.
    pub async fn set_leds(&mut self, duties: &[u8]) -> Result<(), PCA9956Error> {
        if duties.len() > 24 {
            return Err(PCA9956Error::InvalidArgument);
        }
        self.i2c_block_write(Registers::PWM_0, duties).await?;
        Ok(())
    }
}

/// Calculates the value that has to be written into the IREF register of a LED
/// to ensure a maximum current of i_ma through that LED (Datasheet section 7.3.13.1)
fn calculate_iref(r_ext: f32, i_ma: f32) -> u8 {
    ((i_ma * r_ext * 4.0) / 900.0) as u8
}
