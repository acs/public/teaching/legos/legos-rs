#![doc(
    html_logo_url = "../legos_logo_simple.svg",
    html_favicon_url = "../favicon.ico"
)]
#![allow(rustdoc::private_intra_doc_links)]
//! ![LEGOS logo](../legos_logo.svg)
//! # LEGOS core library
//!
//! This crate collects functionalities required in multiple/all LEGOS entities.
//! - [WiFi Configuration](network)
//! - [MQTT Configuration](mqtt)
//! - [File handling](flash_file_reader)
//! - [Device drivers](crate::devices)

#![no_std]

use embassy_executor::Spawner;
use esp_backtrace as _;
use esp_hal::{
    clock::Clocks,
    peripherals::{RADIO_CLK, TIMG1, WIFI},
    rng::Rng,
    timer::{timg::TimerGroup, ErasedTimer, PeriodicTimer},
};
use esp_wifi::{initialize, wifi::WifiStaDevice, EspWifiInitFor};
#[allow(unused_imports)]
use log::{debug, error, info, warn};
use rand_core::RngCore;

pub mod devices;
mod flash_file_reader;
mod http;
pub mod i2c;
mod mqtt;
pub mod network;
pub mod power;

pub use flash_file_reader::FlashFileReader;
use http::init_picoserve;
pub use http::{HtmlConfig, HttpGetCallback, HttpGetConfig, HttpPostCallback, HttpPostConfig};
use mqtt::mqtt_client_task;
pub use mqtt::{
    MQTTConfig, MQTTPublicationConfig, MQTTSubscriptionConfig, MqttPubCallback, MqttSubCallback,
};
use network::{init_network_stack, net_config_print, net_stack_task, wifi_connection_task};
pub use network::{IpConfig, NetworkConfig};

use crate::network::mdns_responder_task;

const WEB_TASK_POOL_SIZE: usize = 4;

#[allow(clippy::too_many_arguments)]
/// initializes common aspects of the legos firmware
/// - Wifi
/// - MQTT
/// - HTTP Server
pub fn init_legos(
    spawner: &Spawner,
    network_cfg: NetworkConfig<'static>,
    mqtt_cfg: MQTTConfig<'static>,
    html_config: HtmlConfig,
    mut rng: Rng,
    wifi: WIFI,
    timg1: TIMG1,
    clocks: &Clocks,
    radio_clock: RADIO_CLK,
) {
    let timg1 = PeriodicTimer::new(Into::<ErasedTimer>::into(
        TimerGroup::new(timg1, clocks).timer0,
    ));

    let init = initialize(EspWifiInitFor::Wifi, timg1, rng, radio_clock, clocks).unwrap();

    info!("Initializing WiFi hardware");
    let (wifi_interface, controller) =
        esp_wifi::wifi::new_with_mode(&init, wifi, WifiStaDevice).unwrap();

    let stack = init_network_stack(wifi_interface, &network_cfg, rng.next_u64());
    info!("MAC address: {:x?}", stack.hardware_address());
    info!("-> Wifi hardware inizialized");

    spawner
        .spawn(wifi_connection_task(
            controller,
            network_cfg.ssid,
            network_cfg.passwd,
        ))
        .ok();
    spawner.spawn(net_stack_task(stack)).ok();
    spawner.spawn(net_config_print(stack)).ok();
    if let IpConfig::Dhcp(hostname) = network_cfg.ip_cfg {
        // Only respond to mdns when using DHCP
        spawner.spawn(mdns_responder_task(stack, hostname)).unwrap();
    }
    spawner.spawn(mqtt_client_task(stack, mqtt_cfg, rng)).ok();
    init_picoserve(spawner, stack, html_config);

    info!("LEGOS Core tasks started");
}
