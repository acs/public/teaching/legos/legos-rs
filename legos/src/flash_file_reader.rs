//! Flash partition data access

use core::cell::RefCell;
use embassy_sync::blocking_mutex::{raw::CriticalSectionRawMutex, Mutex};
use embedded_storage::ReadStorage;
use esp_storage::FlashStorage;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use postcard::from_bytes;
use serde::Deserialize;
use static_cell::StaticCell;

#[derive(Deserialize, Debug, Clone, Copy)]
/// The location of a file in relation to the partition start.
struct FileLocation {
    start: u32,
    len: u32,
}

#[derive(Deserialize, Debug, Clone)]
/// Data structure describing files that are lying as serial bytes on the flash.
pub struct FileList<'a> {
    #[serde(borrow)]
    names: heapless::Vec<&'a str, 32>,
    files: heapless::Vec<FileLocation, 32>,
}

static FLASH: Mutex<CriticalSectionRawMutex, RefCell<Option<FlashStorage>>> =
    Mutex::new(RefCell::new(None));
static FLASH_READER: StaticCell<FlashFileReader> = StaticCell::new();
static mut FILE_LIST_STORAGE: [u8; 256] = [0; 256];

#[derive(Debug, Copy, Clone)]
pub enum FlashFileError {
    FileNotFound,
    FlashReadError,
}

#[derive(Debug)]
/// This provides an interface to the internal Flash of the ESP32, so files can be stored on the partition.
pub struct FlashFileReader {
    flash_addr: u32,
    file_list: FileList<'static>,
}
impl FlashFileReader {
    /// Initializes a new [`FlashFileReader`].
    ///
    /// It expects, that there is a [`FileList`] serialized in the [postcard](https://crates.io/crates/postcard) format stored at `flash_addr`.
    pub fn new(flash_addr: u32) -> &'static Self {
        let mut flash = FlashStorage::new();
        info!("Flash size = {}", flash.capacity());

        // This is unsound, and breaks if FileReader is initialized twice.
        // However, if this happens, the function panics later anyway upon
        // initializing the static cell
        let (file_list, _len): (FileList<'static>, usize) = unsafe {
            flash.read(flash_addr, FILE_LIST_STORAGE.as_mut()).unwrap();
            from_bytes(FILE_LIST_STORAGE.as_ref())
                .expect("Couldn't find files on the storage partition")
        };

        info!("Found the following files on the storage partition:");
        for n in file_list.names.iter() {
            info!(" - {n}");
        }

        FLASH.lock(|f| *f.borrow_mut() = Some(flash));

        FLASH_READER.init(Self {
            flash_addr,
            file_list,
        })
    }

    /// Checks if a file is present on the flash.
    pub fn contains(&self, filename: &str) -> bool {
        for n in self.file_list.names.iter() {
            if *n == filename {
                return true;
            }
        }
        false
    }

    /// Returns the length of a file in bytes.
    pub fn file_len(&self, filename: &str) -> Option<u32> {
        for (n, f) in self.file_list.names.iter().zip(self.file_list.files.iter()) {
            if *n == filename {
                return Some(f.len);
            }
        }
        None
    }

    /// Reads the file `filename` at `offset` into `target`. Returns the amount of bytes written.
    ///
    /// It might be necessary to call this function multiple times for block wise reading if you want to keep the RAM usage low.
    pub fn read_file(
        &self,
        filename: &str,
        target: &mut [u8],
        offset: u32,
    ) -> Result<usize, FlashFileError> {
        for (n, f) in self.file_list.names.iter().zip(self.file_list.files.iter()) {
            if *n == filename {
                let remaining_bytes = (f.len - offset) as usize;
                if remaining_bytes == 0 {
                    return Ok(0);
                }
                let bytes_to_read = if remaining_bytes < target.len() {
                    remaining_bytes
                } else {
                    target.len()
                };
                let read_pos = self.flash_addr + f.start + offset;
                debug!("reading {bytes_to_read} bytes with offset {offset:x} from file {filename} at flash pos {read_pos:x}");
                if let Err(e) = FLASH.lock(|f| {
                    f.borrow_mut()
                        .as_mut()
                        .unwrap()
                        .read(read_pos, &mut target[0..bytes_to_read])
                }) {
                    error!("Flash reading error {e:?}");
                    return Err(FlashFileError::FlashReadError);
                }
                return Ok(bytes_to_read);
            }
        }
        Err(FlashFileError::FileNotFound)
    }
}
