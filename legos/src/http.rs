use embassy_executor::Spawner;
use embassy_net::Stack;
use embassy_time::Duration;
use esp_wifi::wifi::{WifiDevice, WifiStaDevice};
use heapless::{String, Vec};
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use picoserve::{
    request::{Path, Request},
    response::{self, Content, IntoResponse, Response, StatusCode},
    ResponseSent, Router,
};
use static_cell::StaticCell;

use crate::{FlashFileReader, WEB_TASK_POOL_SIZE};

/// A callback for a GET request. The return value is responded to the client.
pub type HttpGetCallback = fn() -> String<512>;
/// A callback for a POST request. The body is passed as argument.
pub type HttpPostCallback = fn(String<512>);

#[derive(Debug, Copy, Clone)]
/// Configures a callback that is triggered on a GET request on `path`.
pub struct HttpGetConfig {
    pub callback: HttpGetCallback,
    pub path: &'static str,
}

#[derive(Debug, Copy, Clone)]
/// Configures a callback that is triggered by a POST request on `path`.
pub struct HttpPostConfig {
    pub callback: HttpPostCallback,
    pub path: &'static str,
}

#[derive(Debug, Copy, Clone)]
pub struct HtmlConfig {
    pub html_files: &'static FlashFileReader,
    pub get_config: Option<HttpGetConfig>,
    pub post_config: Option<HttpPostConfig>,
}

static PAGE_404: &str = r#"<html><head>
 <link rel="stylesheet" href="css/w3.css">
 <header class="w3-container w3-border-bottom w3-border-orange">
  <div class="w3-row">
   <div class="w3-left">
    <h3 class="w3-opacity""><b>404 Not Found</b></h3>
   </div>
  </div>
 </header>
 <body>
  <div class="w3-display-container w3-animate-opacity w3-center">
   <div class="w3-row w3-margin-bottom w3-padding">
    <div class="w3-left">
  Invalid Page
    </div>
   </div>
  </div>
 </body>
</html>"#;

fn get_content_type(filename: &str) -> Option<&'static str> {
    let file_ending = filename.split('.').last()?;
    debug!("file ending: {file_ending}");
    Some(match file_ending {
        "css" => "text/css",
        "html" => "text/html",
        "ico" => "text/x-icon",
        "jpeg" => "image/jpeg",
        "pdf" => "application/pdf",
        "png" => "image/png",
        "svg" => "image/svg+xml",
        _ => "text/plain",
    })
}

struct FileReaderService<'a> {
    flash_reader: &'static FlashFileReader,
    filename: &'a str,
}

impl<'a> Content for FileReaderService<'a> {
    fn content_type(&self) -> &'static str {
        get_content_type(self.filename).unwrap()
    }
    fn content_length(&self) -> usize {
        self.flash_reader.file_len(self.filename).unwrap() as usize
    }
    async fn write_content<
        R: embedded_io_async::Read,
        W: embedded_io_async::Write<Error = R::Error>,
    >(
        self,
        _connection: response::Connection<'_, R>,
        mut writer: W,
    ) -> Result<(), W::Error> {
        let mut tmp_buf = [0; 1024];
        let mut offset = 0;

        loop {
            let written_bytes = self
                .flash_reader
                .read_file(self.filename, &mut tmp_buf, offset)
                .unwrap();
            //debug!("written_bytes: {:x?}", &tmp_buf[0..written_bytes]);
            writer.write_all(&tmp_buf[0..written_bytes]).await?;
            offset += written_bytes as u32;
            if written_bytes == 0 {
                break;
            }
        }
        Ok(())
    }
}

struct FileProviderService<'a> {
    config: &'a HtmlConfig,
}

impl<'a, STATE, PP> picoserve::routing::PathRouterService<STATE, PP> for FileProviderService<'a> {
    async fn call_request_handler_service<
        R: picoserve::io::Read,
        W: picoserve::response::ResponseWriter<Error = R::Error>,
    >(
        &self,
        _state: &STATE,
        _path_params: PP,
        path: Path<'_>,
        mut request: Request<'_, R>,
        response_writer: W,
    ) -> Result<ResponseSent, W::Error> {
        trace!("GET request for {path}, method: {}", request.parts.method());
        let mut path_s = String::<256>::try_from(&path.encoded()[1..]).unwrap();

        if path_s.len() == 0 || path_s.as_bytes()[path_s.len() - 1] == b'/' {
            // same as above
            path_s.push_str("index.html").ok();
        }

        if let Some(cb_conf) = &self.config.post_config {
            if path_s == cb_conf.path && request.parts.method() == "POST" {
                if request.body_connection.content_length() > 512 {
                    return response::File::html(PAGE_404)
                        .into_response()
                        .with_status_code(StatusCode::new(404))
                        .write_to(request.body_connection.finalize().await?, response_writer)
                        .await;
                }
                let mut v = Vec::new();
                v.extend_from_slice(request.body_connection.body().read_all().await.unwrap())
                    .unwrap();
                let s = String::<512>::from_utf8(v).unwrap();
                (cb_conf.callback)(s);
                return Response::new(StatusCode::new(200), "OK")
                    .write_to(request.body_connection.finalize().await?, response_writer)
                    .await;
            }
        }
        if let Some(cb_conf) = &self.config.get_config {
            if path_s == cb_conf.path {
                let res = (cb_conf.callback)();
                let resp = Response::new(StatusCode::new(200), res.as_bytes());
                return resp
                    .write_to(request.body_connection.finalize().await?, response_writer)
                    .await;
            }
        }

        let resp = if self.config.html_files.contains(path_s.as_str()) {
            // we can unwrap, as long as all files in our struct have one of the valid file extensions
            let resp = Response::new(
                StatusCode::new(200),
                FileReaderService {
                    flash_reader: self.config.html_files,
                    filename: path_s.as_str(),
                },
            );
            return resp
                .write_to(request.body_connection.finalize().await?, response_writer)
                .await;
        } else {
            response::File::html(PAGE_404)
                .into_response()
                .with_status_code(StatusCode::new(404))
        };
        resp.write_to(request.body_connection.finalize().await?, response_writer)
            .await
    }
}

static PICOSERVE_CONFIG: StaticCell<picoserve::Config<Duration>> = StaticCell::new();

pub(crate) fn init_picoserve(
    spawner: &Spawner,
    stack: &'static Stack<WifiDevice<'static, WifiStaDevice>>,
    html_config: HtmlConfig,
) {
    let config = &*PICOSERVE_CONFIG.init(
        picoserve::Config::new(picoserve::Timeouts {
            start_read_request: Some(Duration::from_secs(5)),
            read_request: Some(Duration::from_secs(1)),
            write: Some(Duration::from_secs(1)),
        })
        .keep_connection_alive(),
    );

    info!("Starting HTTP Server");
    for id in 0..WEB_TASK_POOL_SIZE {
        spawner.must_spawn(web_task(id, stack, config, html_config));
    }
}

#[embassy_executor::task(pool_size = WEB_TASK_POOL_SIZE)]
async fn web_task(
    id: usize,
    stack: &'static Stack<WifiDevice<'static, WifiStaDevice>>,
    config: &'static picoserve::Config<Duration>,
    html_config: HtmlConfig,
) -> ! {
    let port = 80;
    let mut tcp_rx_buffer = [0; 1536];
    let mut tcp_tx_buffer = [0; 1536];
    let mut http_buffer = [0; 1536];

    let app = Router::from_service(FileProviderService {
        config: &html_config,
    });

    picoserve::listen_and_serve(
        id,
        &app,
        config,
        stack,
        port,
        &mut tcp_rx_buffer,
        &mut tcp_tx_buffer,
        &mut http_buffer,
    )
    .await
}
