use core::fmt::Write;
use embedded_hal_async::i2c::I2c;
use heapless::String;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

/// Scans the I2C bus and prints a map of all found devices
pub async fn bus_map<I2C: I2c>(i2c: &mut I2C) {
    let mut results = [-1; 16 * 8];
    for (addr, res) in results.iter_mut().enumerate() {
        if let Ok(()) = i2c.read(addr as u8, &mut [0; 0]).await {
            *res = addr as i8;
        }
    }
    info!("I2C Map:");
    info!("      0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f");
    for i in 0..8 {
        let mut res_string = String::<64>::new();
        write!(res_string, "{:#4x} ", i * 16).unwrap();
        for r in &results[i * 16..(i + 1) * 16] {
            if *r != -1 {
                write!(res_string, " {:2x}", r).unwrap();
            } else {
                write!(res_string, " --").unwrap();
            }
        }
        info!("{res_string}");
    }
}

/// Resets all devices on the I2C-bus that support the general call adress (see I2C-bus specification 3.1.13)
pub async fn reset_devices<I2C: I2c>(i2c: &mut I2C) {
    i2c.write(0, &[0b110]).await.unwrap();
}
