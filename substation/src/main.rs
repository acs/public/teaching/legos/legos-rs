#![doc(
    html_logo_url = "../legos_logo_simple.svg",
    html_favicon_url = "../favicon.ico"
)]
//! # LEGOS Substation
//!
//! The substation connects _high voltage_ branches with regular branches. It can react to overcurrent interrupts from the branches and additionally has it's own power measurement & switch.

#![no_std]
#![no_main]

use constcat::concat;
use core::{cell::RefCell, fmt::Write};
use embassy_embedded_hal::shared_bus::asynch::i2c::I2cDevice;
use embassy_executor::Spawner;
use embassy_futures::select::{select, select3, select4, Either, Either3, Either4};
use embassy_net::{IpAddress, IpEndpoint};
use embassy_sync::{
    blocking_mutex::{raw::CriticalSectionRawMutex, Mutex as BlockingMutex},
    channel::Channel,
    mutex::Mutex,
};
use embassy_time::{Duration, Ticker, Timer};
use esp_hal::{
    clock::{ClockControl, Clocks},
    gpio::{GpioPin, Input, Io, Level, Output, Pins, Pull},
    i2c::I2C,
    peripherals::{Peripherals, I2C0},
    prelude::*,
    rng::Rng,
    system::SystemControl,
    timer::{timg::TimerGroup, ErasedTimer, OneShotTimer},
    Async,
};
use esp_println::logger::init_logger;
use heapless::{HistoryBuffer, String};
use legos::{devices::ina233::AlertType, power::PowerReading};
use legos::{
    devices::ina233::INA233,
    i2c::bus_map,
    init_legos,
    network::{str_to_ipv4_cidr, str_to_ipv4_port},
    FlashFileReader, HtmlConfig, HttpGetConfig, HttpPostConfig, IpConfig, MQTTConfig,
    MQTTPublicationConfig, MQTTSubscriptionConfig, NetworkConfig,
};
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use num_traits::float::FloatCore;
use serde::{Deserialize, Serialize};
use serde_json_core::{from_str, to_string};
use static_cell::StaticCell;
use static_toml::static_toml;

static_toml! {
    const CONFIG = include_toml!("../legos_cfg.toml");
}

const HOSTNAME: &str = concat!(CONFIG.substation.hostname, "-", {
    // unwrap is not const...
    match option_env!("SUBSTATION_NR") {
        Some(s) => s,
        None => "1",
    }
});
const CLIENT_NAME: &str = concat!("substation", {
    // unwrap is not const...
    match option_env!("SUBSTATION_NR") {
        Some(s) => s,
        None => "1",
    }
});

static MQTT_TOPIC_PUB: StaticCell<String<64>> = StaticCell::new();
static MQTT_TOPIC_SUB: StaticCell<String<64>> = StaticCell::new();

// General tasks (e.g, sensor reading) are executed at this interval
const UPDATE_INTERVAL: Duration = Duration::from_millis(333);

const CURRENT_LIMIT: f32 = 4.0;
const VOLTAGE_LIMIT: f32 = 4.0;

#[derive(Debug, Copy, Clone, Serialize)]
/// Current state of the substation. Used for control algorithms and external
/// reporting (MQTT/HTTP).
struct SubstationState {
    power: PowerReading,
    power_perc: f32,
    enabled: bool,
    bus_1_enabled: bool,
    bus_2_enabled: bool,
    bus_3_enabled: bool,
    bus_4_enabled: bool,
    bus_1_irq: bool,
    bus_2_irq: bool,
    bus_3_irq: bool,
    bus_4_irq: bool,
    overcurrent: bool,
    overvoltage: bool,
    override_active: bool,
}
impl SubstationState {
    const fn new() -> Self {
        Self {
            power: PowerReading {
                voltage: 0.0,
                current: 0.0,
                power: 0.0,
            },
            power_perc: 0.0,
            enabled: true,
            bus_1_enabled: true,
            bus_2_enabled: true,
            bus_3_enabled: true,
            bus_4_enabled: true,
            bus_1_irq: false,
            bus_2_irq: false,
            bus_3_irq: false,
            bus_4_irq: false,
            overcurrent: false,
            overvoltage: false,
            override_active: false,
        }
    }
}

#[derive(Debug, Copy, Clone, Deserialize, Default)]
/// Datastructure for overriding control in [`Substation`]. Generated externally
/// (MQTT/HTTP)
struct SubstationExternalControl {
    bus_enabled: Option<bool>,
    enabled_toggle: Option<bool>,
    branch1_enabled: Option<bool>,
    branch2_enabled: Option<bool>,
    branch3_enabled: Option<bool>,
    branch4_enabled: Option<bool>,
    branch1_toggle: Option<bool>,
    branch2_toggle: Option<bool>,
    branch3_toggle: Option<bool>,
    branch4_toggle: Option<bool>,
}

#[derive(Debug, Copy, Clone)]
struct PreOverrideState {
    bus_enable: bool,
    not_en1: bool,
    not_en2: bool,
    not_en3: bool,
    not_en4: bool,
}

static SUBSTATION_STATUS: BlockingMutex<CriticalSectionRawMutex, RefCell<SubstationState>> =
    BlockingMutex::new(RefCell::new(SubstationState::new()));
static I2C_BUS: StaticCell<Mutex<CriticalSectionRawMutex, I2C<'_, I2C0, Async>>> =
    StaticCell::new();

static CONTROL_CHANNEL: Channel<CriticalSectionRawMutex, SubstationExternalControl, 4> =
    Channel::new();

/// Holds the handles to the substation hardware
struct Substation {
    ina233: INA233<I2cDevice<'static, CriticalSectionRawMutex, I2C<'static, I2C0, Async>>>,
    // Irq pins from the branches
    irq1: Input<'static, GpioPin<4>>,
    irq2: Input<'static, GpioPin<9>>,
    irq3: Input<'static, GpioPin<10>>,
    irq4: Input<'static, GpioPin<12>>,
    // Pins for disabling one of the branches
    not_en1: Output<'static, GpioPin<13>>,
    not_en2: Output<'static, GpioPin<14>>,
    not_en3: Output<'static, GpioPin<15>>,
    not_en4: Output<'static, GpioPin<18>>,
    // Pins for "burning" resistors
    _fault1: Output<'static, GpioPin<19>>,
    _fault2: Output<'static, GpioPin<21>>,
    _fault3: Output<'static, GpioPin<22>>,
    _fault4: Output<'static, GpioPin<23>>,
    pwr_int: Input<'static, GpioPin<26>>,
    not_bus_enable: Output<'static, GpioPin<27>>,
    voltage_history: HistoryBuffer<f32, 4>,
    current_history: HistoryBuffer<f32, 4>,
    power_history: HistoryBuffer<f32, 4>,
    override_state: Option<(Duration, PreOverrideState)>,
    fault_protection_active: Option<(Duration, bool)>,
}
impl Substation {
    async fn new(mut pins: Pins, i2c: I2C0, clocks: &Clocks<'static>) -> Self {
        // Trick: the IRQs are actually inputs, but have a fairly high pull down
        // on the branches. Setting them high does not affect the output of the
        // INAs on the branches. So we "abuse" them for a startup animation
        let mut irq1 = Output::new(&mut pins.gpio4, Level::High);
        let mut irq2 = Output::new(&mut pins.gpio9, Level::High);
        let mut irq3 = Output::new(&mut pins.gpio10, Level::High);
        let mut irq4 = Output::new(&mut pins.gpio12, Level::High);
        startup_animation(&mut irq1, &mut irq2, &mut irq3, &mut irq4).await;

        info!("Starting I2C");
        let sda = pins.gpio25;
        let scl = pins.gpio33;
        let mut i2c_bus = I2C::new_async(i2c, sda, scl, 400.kHz(), clocks);
        bus_map(&mut i2c_bus).await;
        let i2c_bus_mutex = Mutex::<CriticalSectionRawMutex, _>::new(i2c_bus);
        let i2c_bus_static = I2C_BUS.init(i2c_bus_mutex);
        let mut ina233 = INA233::new(I2cDevice::new(i2c_bus_static), 0x40, 0.010, 8.0)
            .await
            .unwrap();

        ina233
            .set_alert(CURRENT_LIMIT, VOLTAGE_LIMIT)
            .await
            .unwrap();

        let irq1 = Input::new(pins.gpio4, Pull::Up);
        let irq2 = Input::new(pins.gpio9, Pull::Up);
        let irq3 = Input::new(pins.gpio10, Pull::Up);
        let irq4 = Input::new(pins.gpio12, Pull::Up);
        let not_en1 = Output::new(pins.gpio13, Level::Low);
        let not_en2 = Output::new(pins.gpio14, Level::Low);
        let not_en3 = Output::new(pins.gpio15, Level::Low);
        let not_en4 = Output::new(pins.gpio18, Level::Low);
        let _fault1 = Output::new(pins.gpio19, Level::Low);
        let _fault2 = Output::new(pins.gpio21, Level::Low);
        let _fault3 = Output::new(pins.gpio22, Level::Low);
        let _fault4 = Output::new(pins.gpio23, Level::Low);
        let pwr_int = Input::new(pins.gpio26, Pull::Up);
        let bus_enable = Output::new(pins.gpio27, Level::Low);
        Self {
            irq1,
            irq2,
            irq3,
            irq4,
            not_en1,
            not_en2,
            not_en3,
            not_en4,
            _fault1,
            _fault2,
            _fault3,
            _fault4,
            pwr_int,
            not_bus_enable: bus_enable,
            ina233,
            voltage_history: HistoryBuffer::new(),
            current_history: HistoryBuffer::new(),
            power_history: HistoryBuffer::new(),
            override_state: None,
            fault_protection_active: None,
        }
    }

    async fn update_power_vals(&mut self) {
        if let Ok(pwr) = self.ina233.read_power().await {
            self.voltage_history.write(pwr.voltage);
            self.current_history.write(-pwr.current);
            self.power_history.write(pwr.power);

            if pwr.voltage < VOLTAGE_LIMIT && pwr.current.abs() < CURRENT_LIMIT {
                self.ina233.clear_alert().await.unwrap();
            }

            let voltage_avg = self.voltage_history.as_slice().iter().sum::<f32>()
                / self.voltage_history.len() as f32;
            let current_avg = self.current_history.as_slice().iter().sum::<f32>()
                / self.current_history.len() as f32;
            let power_avg =
                self.power_history.as_slice().iter().sum::<f32>() / self.power_history.len() as f32;

            SUBSTATION_STATUS.lock(|s| {
                s.borrow_mut().power = PowerReading {
                    voltage: voltage_avg,
                    current: current_avg,
                    power: power_avg,
                };
                s.borrow_mut().power_perc = (current_avg.abs() / 8.0).clamp(0.0, 100.0);
            });
        } else {
            error!("Reading from INA failed");
        }
    }

    fn print_status(&self) {
        let s = SUBSTATION_STATUS.lock(|s| *s.borrow());
        debug!("{s:#?}");
    }

    fn read_state(&self) -> PreOverrideState {
        PreOverrideState {
            bus_enable: self.not_bus_enable.is_set_high(),
            not_en1: self.not_en1.is_set_high(),
            not_en2: self.not_en2.is_set_high(),
            not_en3: self.not_en3.is_set_high(),
            not_en4: self.not_en4.is_set_high(),
        }
    }

    fn apply_external_control(&mut self, control: &SubstationExternalControl) {
        info!("Applying override");

        if let Some((_duration, state)) = self.override_state {
            self.override_state = Some((
                Duration::from_secs(CONFIG.general.override_duration as u64),
                state,
            ));
        } else {
            self.override_state = Some((
                Duration::from_secs(CONFIG.general.override_duration as u64),
                self.read_state(),
            ));
        }

        if let Some(enabled_toggle) = control.enabled_toggle {
            if enabled_toggle {
                self.not_bus_enable.toggle()
            }
        }
        if let Some(bus_enabled) = control.bus_enabled {
            self.not_bus_enable.set_level(Level::from(!bus_enabled));
        }

        if let Some(branch1_toggle) = control.branch1_toggle {
            if branch1_toggle {
                self.not_en1.toggle()
            }
        }
        if let Some(branch1_enabled) = control.branch1_enabled {
            self.not_en1.set_level((!branch1_enabled).into());
        }

        if let Some(branch2_toggle) = control.branch2_toggle {
            if branch2_toggle {
                self.not_en2.toggle()
            }
        }
        if let Some(branch2_enabled) = control.branch2_enabled {
            self.not_en2.set_level((!branch2_enabled).into());
        }

        if let Some(branch3_toggle) = control.branch3_toggle {
            if branch3_toggle {
                self.not_en3.toggle()
            }
        }
        if let Some(branch3_enabled) = control.branch3_enabled {
            self.not_en3.set_level((!branch3_enabled).into());
        }

        if let Some(branch4_toggle) = control.branch4_toggle {
            if branch4_toggle {
                self.not_en4.toggle()
            }
        }
        if let Some(branch4_enabled) = control.branch4_enabled {
            self.not_en4.set_level((!branch4_enabled).into());
        }
    }

    fn start_fault_protection(&mut self) {
        info!("Fault protection active");
        self.fault_protection_active = Some((
            Duration::from_secs(CONFIG.general.override_duration as u64),
            self.not_bus_enable.is_set_high(),
        ));
        self.not_bus_enable.set_high();
    }

    fn end_fault_protection(&mut self) {
        info!("Ending Fault Protection");
        let (_duration, prev_state) = self.fault_protection_active.unwrap();
        self.not_bus_enable.set_level(prev_state.into());
        self.fault_protection_active = None;
    }

    async fn update_status(&mut self) {
        let alert = if self.pwr_int.is_low() && self.fault_protection_active.is_none() {
            let alert = self.ina233.check_alert().await.unwrap();
            error!("Bus alert: {alert:?}");
            self.start_fault_protection();
            Some(alert)
        } else {
            None
        };

        SUBSTATION_STATUS.lock(|s| {
            let mut s = s.borrow_mut();
            s.bus_1_irq = self.irq1.is_low();
            s.bus_2_irq = self.irq2.is_low();
            s.bus_3_irq = self.irq3.is_low();
            s.bus_4_irq = self.irq4.is_low();
            s.bus_1_enabled = !self.not_en1.is_set_high();
            s.bus_2_enabled = !self.not_en2.is_set_high();
            s.bus_3_enabled = !self.not_en3.is_set_high();
            s.bus_4_enabled = !self.not_en4.is_set_high();
            s.enabled = !self.not_bus_enable.is_set_high();
            s.override_active = self.override_state.is_some();
            if let Some(alert) = alert {
                s.overcurrent = alert == AlertType::Overcurrent;
                s.overvoltage = alert == AlertType::Overvoltage;
            } else {
                s.overcurrent = false;
                s.overvoltage = false;
            }
        });
    }

    fn end_external_control(&mut self) {
        info!("Ending override");
        let (_duration, pre_state) = self.override_state.unwrap();
        self.not_bus_enable.set_level(pre_state.bus_enable.into());
        self.not_en1.set_level(pre_state.not_en1.into());
        self.not_en2.set_level(pre_state.not_en2.into());
        self.not_en3.set_level(pre_state.not_en3.into());
        self.not_en4.set_level(pre_state.not_en4.into());
        self.override_state = None;
    }

    pub async fn run(&mut self) {
        let mut ticker = Ticker::every(UPDATE_INTERVAL);
        let mut i: i32 = 0;
        loop {
            match select3(
                ticker.next(),
                CONTROL_CHANNEL.receive(),
                select(
                    select4(
                        self.irq1.wait_for_low(),
                        self.irq2.wait_for_low(),
                        self.irq3.wait_for_low(),
                        self.irq4.wait_for_low(),
                    ),
                    self.pwr_int.wait_for_low(),
                ),
            )
            .await
            {
                Either3::First(_timeout) => {
                    self.update_power_vals().await;
                    if let Some((duration, _state)) = self.override_state.as_mut() {
                        if *duration >= UPDATE_INTERVAL {
                            *duration -= UPDATE_INTERVAL;
                        } else {
                            self.end_external_control();
                            Timer::after(Duration::from_millis(1)).await;
                        }
                    }
                    if let Some((duration, _state)) = self.fault_protection_active.as_mut() {
                        if *duration >= UPDATE_INTERVAL {
                            *duration -= UPDATE_INTERVAL;
                        } else {
                            self.end_fault_protection();
                            Timer::after(Duration::from_millis(1)).await;
                        }
                    }
                    self.update_status().await;
                    i = i.wrapping_add(1);
                    if i % 20 == 0 {
                        self.print_status();
                    }
                }
                Either3::Second(control) => {
                    info!("Applying control struct {control:?}");
                    self.apply_external_control(&control);
                    Timer::after(Duration::from_millis(1)).await;
                    self.update_status().await;
                }
                Either3::Third(control) => {
                    // This interrupt also triggers on rising flanks
                    let (irq_num, actually_low) = match control {
                        Either::First(irq_control) => match irq_control {
                            Either4::First(_) => (1, self.irq1.is_low()),
                            Either4::Second(_) => (2, self.irq2.is_low()),
                            Either4::Third(_) => (3, self.irq3.is_low()),
                            Either4::Fourth(_) => (4, self.irq4.is_low()),
                        },
                        Either::Second(_) => (5, self.pwr_int.is_low()),
                    };

                    if actually_low {
                        trace!("Interrupt nr {irq_num}");
                        self.update_status().await;
                        // Add a debouncing timer
                        Timer::after(Duration::from_millis(100)).await;
                    }
                }
            }
        }
    }
}

async fn startup_animation(
    pin1: &mut Output<'_, GpioPin<4>>,
    pin2: &mut Output<'_, GpioPin<9>>,
    pin3: &mut Output<'_, GpioPin<10>>,
    pin4: &mut Output<'_, GpioPin<12>>,
) {
    let duration = Duration::from_millis(100);
    pin1.set_high();
    pin2.set_high();
    pin3.set_high();
    pin4.set_high();
    Timer::after(Duration::from_millis(200)).await;
    for _ in 0..2 {
        pin1.set_low();
        pin4.set_high();
        Timer::after(duration).await;
        pin2.set_low();
        pin1.set_high();
        Timer::after(duration).await;
        pin3.set_low();
        pin2.set_high();
        Timer::after(duration).await;
        pin4.set_low();
        pin3.set_high();
        Timer::after(duration).await;
    }
    pin1.set_high();
    pin2.set_high();
    pin3.set_high();
    pin4.set_high();
}

#[embassy_executor::task]
async fn run(mut substation: Substation) {
    substation.run().await
}

fn mqtt_pub_callback() -> Option<String<1024>> {
    let status = SUBSTATION_STATUS.lock(|s| *s.borrow());
    // serde_json_core produces a heapless v0.7 String but we need a heapless v0.8 String, therefore the conversion
    Some(String::try_from(to_string::<_, 256>(&status).ok()?.as_str()).unwrap())
}

fn mqtt_sub_callback(topic: &str, message: &str) {
    info!("mqtt_sub_callback on topic {topic} with msg {message}");
    match from_str(message) {
        Ok((control, _nr_bytes)) => {
            if let Err(e) = CONTROL_CHANNEL.try_send(control) {
                error!("Too many control requests at once: {e:?}");
            }
        }
        Err(e) => {
            error!("invalid control JSON: {e}")
        }
    }
}

fn html_status_callback() -> String<512> {
    trace!("html status callback");
    let status = SUBSTATION_STATUS.lock(|s| *s.borrow());
    // serde_json_core produces a heapless v0.7 String but we need a heapless v0.8 String, therefore the conversion
    String::try_from(to_string::<_, 512>(&status).ok().unwrap().as_str()).unwrap()
}

fn html_override_callback(s: String<512>) {
    info!("html override: {s}");
    match from_str(s.as_str()) {
        Ok((control, _nr_bytes)) => {
            if let Err(e) = CONTROL_CHANNEL.try_send(control) {
                error!("Too many control requests at once: {e:?}");
            }
        }
        Err(e) => {
            error!("invalid control JSON: {e}")
        }
    }
}

#[main]
async fn main(spawner: Spawner) {
    let peripherals = Peripherals::take();
    let system = SystemControl::new(peripherals.SYSTEM);

    let clocks_initialized = ClockControl::max(system.clock_control).freeze();
    let timg0 = TimerGroup::new(peripherals.TIMG0, &clocks_initialized);
    let timer0: ErasedTimer = timg0.timer0.into();
    static TIMERS: StaticCell<[OneShotTimer<ErasedTimer>; 1]> = StaticCell::new();
    let timers = TIMERS.init([OneShotTimer::new(timer0)]);
    esp_hal_embassy::init(&clocks_initialized, timers);

    init_logger(log::LevelFilter::Debug);
    info!("Logger is setup");

    let rng = Rng::new(peripherals.RNG);

    let io = Io::new(peripherals.GPIO, peripherals.IO_MUX);
    let substation = Substation::new(io.pins, peripherals.I2C0, &clocks_initialized).await;

    // === Network Configuration ===
    let ip_cfg = match CONFIG.substation.ip_addr {
        "dhcp" => IpConfig::Dhcp(HOSTNAME),
        addr => IpConfig::StaticV4(str_to_ipv4_cidr(addr).expect("Invalid IP Address"), None),
    };
    let network_cfg = NetworkConfig {
        ssid: CONFIG.wifi.ssid,
        passwd: CONFIG.wifi.passwd,
        ip_cfg,
    };

    // === MQTT Configuration ===
    let (broker_ip, broker_port) =
        str_to_ipv4_port(CONFIG.mqtt.broker).expect("Invalid MQTT broker address");
    let topic_pub = MQTT_TOPIC_PUB.init({
        let mut s = String::<64>::new();
        write!(s, "{}/{CLIENT_NAME}", CONFIG.mqtt.topic).unwrap();
        s
    });
    let topic_sub = MQTT_TOPIC_SUB.init({
        let mut s = String::<64>::new();
        write!(s, "{}/{CLIENT_NAME}/cmd", CONFIG.mqtt.topic).unwrap();
        s
    });
    let mqtt_config = MQTTConfig {
        client_id: CLIENT_NAME,
        broker: IpEndpoint::new(IpAddress::Ipv4(broker_ip), broker_port),
        pub_config: MQTTPublicationConfig {
            topic_pub,
            pub_interval: Duration::from_secs(1),
            callback: mqtt_pub_callback,
        },
        sub_config: Some(MQTTSubscriptionConfig {
            topic_sub,
            callback: mqtt_sub_callback,
        }),
    };

    // === HTTP Server Configuration ===
    let html_files = FlashFileReader::new(0x110000);
    let http_config = HtmlConfig {
        html_files,
        get_config: Some(HttpGetConfig {
            callback: html_status_callback,
            path: "status",
        }),
        post_config: Some(HttpPostConfig {
            callback: html_override_callback,
            path: "override",
        }),
    };

    // === Start the tasks ===
    init_legos(
        &spawner,
        network_cfg,
        mqtt_config,
        http_config,
        rng,
        peripherals.WIFI,
        peripherals.TIMG1,
        &clocks_initialized,
        peripherals.RADIO_CLK,
    );
    spawner.spawn(run(substation)).ok();
}
