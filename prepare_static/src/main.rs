use clap::Parser;
use postcard::to_allocvec;
use serde::{Deserialize, Serialize};
use std::fs::{canonicalize, read_dir, File};
use std::io::prelude::*;
use std::io::BufReader;
use std::path::{Path, PathBuf};

/// Reads and serializes a folder to the "postcard" format. Can be used to create a binary for flashing on a data partition.
#[derive(Parser)]
#[command(version, about, long_about = None)]
struct Args {
    /// The folder to be serialized
    input: std::path::PathBuf,
    /// Resulting serialized file
    output: std::path::PathBuf,
}

fn get_files<P: AsRef<Path>>(dir: P) -> Vec<PathBuf> {
    let mut files = vec![];
    let listing: Vec<_> = read_dir(dir)
        .expect("could not read directory")
        .map(|entry| entry.unwrap().path())
        .collect();
    for path in listing {
        if path.is_file() {
            files.push(path)
        } else if path.is_dir() {
            for file in get_files(&path) {
                files.push(file)
            }
        }
    }
    files
}

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
struct FilePos {
    start: u32,
    len: u32,
}

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
struct FileList<'a> {
    #[serde(borrow)]
    names: heapless::Vec<&'a str, 32>,
    files: heapless::Vec<FilePos, 32>,
}

fn main() {
    let args = Args::parse();
    let dir = args.input;
    let paths = get_files(&dir);
    println!("Creating file map of the following files: {paths:#?}");

    let keys: heapless::Vec<&str, 32> = paths
        .iter()
        .map(|path| path.strip_prefix(&dir).unwrap().to_str().unwrap())
        .collect();

    let vals: Vec<_> = paths
        .iter()
        .map(|path| canonicalize(path).expect("not found"))
        .collect();

    let mut file_list = FileList {
        names: keys.clone(),
        files: heapless::Vec::new(),
    };

    let mut file_bytes = Vec::new();
    let mut pos: u32 = 0;
    for val in vals.iter() {
        let mut buf = Vec::new();
        let file = File::open(val).unwrap();
        let mut buf_reader = BufReader::new(file);
        buf_reader.read_to_end(&mut buf).unwrap();
        file_bytes.extend_from_slice(&buf);
        let file_len = buf.len() as u32;
        file_list
            .files
            .push(FilePos {
                start: pos,
                len: file_len,
            })
            .unwrap();
        pos += file_len;
    }
    // The length of the serialized file position list changes as we modify the postitions. We do it until it stabilizes.
    let mut old_pos = 0;
    let mut new_pos = to_allocvec(&file_list).unwrap().len() as u32 + 1;
    println!("new_pos: {new_pos}, delta {}", new_pos - old_pos);
    while new_pos - old_pos != 0 {
        for f in file_list.files.iter_mut() {
            f.start += new_pos - old_pos;
        }
        old_pos = new_pos;
        new_pos = to_allocvec(&file_list).unwrap().len() as u32 + 1;
        println!("new_pos: {new_pos}, delta {}", new_pos - old_pos);
    }
    println!("file_list: {file_list:#x?}");

    let mut serialized_bytes = to_allocvec(&file_list).unwrap();
    serialized_bytes.push(0);
    serialized_bytes.extend_from_slice(&file_bytes);
    let mut file = File::create(args.output).unwrap();
    file.write_all(&serialized_bytes).unwrap();
}
